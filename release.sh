#!/bin/sh

PROJECTS=`ls config/sites/production_*.yml | sed 's/.*_\(.*\)\.yml/\1/'`

contains() {
  for word in $1; do
    [ $word = $2 ] && return 0
  done
  return 1
}

SITE=$1
if [ "_$SITE" = "_" ]; then
  SITE="_"
fi

contains "$PROJECTS" $SITE
if [ $? -eq 1 ]; then
  echo "Usage: $0 [site]"
  exit 1
fi

git pull
git submodule foreach git pull origin master
bundle install --without development test
SITE=$SITE RAILS_ENV=production rake db:migrate
rm -rf public/assets/*
SITE=$SITE RAILS_ENV=production rake assets:precompile
touch tmp/restart.txt

exit 0