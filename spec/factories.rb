# coding: utf-8

# Factory definitions
FactoryGirl.define do

  sequence :name do |n|; "name_#{n}"; end
  sequence :body do |n|; "body #{n}" * (n+3 % 5) ; end
  sequence :user do |n|; "user#{n}" ; end
  sequence :email do |n|; "user#{n}@example.com" ; end
  sequence :guid do |n|; "deadbeef#{n}" ; end
  sequence :label do |n|; "lab_#{n}" ; end
  sequence :file_name do |f|; "file_name_#{f}" ; end
  sequence :category do |n|; "c_#{n}" ; end
  sequence :time do |n|; DateTime.new(2012,3,26,19,56) - n ; end

  factory :user do
    login { FactoryGirl.generate(:user) }
    email { generate(:email) }
    username 'bond'
    name "James Bond"
    notify_via_email false
    notify_on_new_articles false
    password 'top-secret'
#    association :resource, factory: :avatar
    state 'active'
#    twitter '@getpublify'
    profile
  end

  factory :user_with_an_empty_profile, parent: :user do |u|
    u.username "doe"
    u.name "John Doe"
#    u.twitter nil
    u.association :resource, nil
  end

  factory :user_with_a_full_profile, parent: :user do |u|
    u.description "I am a poor lonesone factory generated user"
    u.url "http://myblog.net"
#    u.twitter "@random"
  end

  factory :article do
    title 'A big article'
    body 'A content with several data'
    guid
    slug 'a-big-article'
    published_at DateTime.new(2005,1,1,2,0,0)
    user
    categories []
    tags []
    allow_comments true
    published true
  end

  factory :unpublished_article, :parent => :article do |a|
    a.published_at nil
    a.published false
  end

  factory :articles do
  end

  factory :post_type do |p|
    p.name 'foobar'
    p.description "Some description"
  end

  factory :utf8article, :parent => :article do |u|
    u.title 'ルビー'
    u.slug 'ルビー'
  end

  factory :second_article, :parent => :article do |a|
    a.title 'Another big article'
  end

  factory :article_with_accent_in_html, :parent => :article do |a|
    a.title 'article with accent'
    a.body '&eacute;coute The future is cool!'
    a.slug 'article-with-accent'
  end

  factory :blog do
    base_url 'http://myblog.net'
    hide_extended_on_rss true
    blog_name 'test blog'
    limit_article_display 2
    blog_subtitle "test subtitles"
    limit_rss_display 10
    email_from "scott@sigkill.org"
    link_to_author false
#    permalink_format "/%year%/%month%/%day%/%title%"
    use_canonical_url true

    after :stub do |blog|
      Blog.stub(:default) { blog }
      [blog.text_filter, blog.comment_text_filter].uniq.each do |filter|
        build_stubbed filter
      end
    end
  end
  #
  #factory :profile, :class => :profile do |l|
  #  l.label {FactoryGirl.generate(:label)}
  #  l.nicename 'Poffy contributor'
  #  l.modules [:dashboard, :profile]
  #end
  #
  #factory :profile_admin, parent: :profile do
  #  label Profile::ADMIN
  #  nicename 'Poffy administrator'
  #  modules [ :dashboard, :write, :articles, :pages, :feedback, :themes,
  #            :customizesidebar, :users, :seo, :media, :settings, :profile ]
  #end
  #
  #factory :profile_publisher, :parent => :profile do |l|
  #  l.label 'publisher'
  #  l.nicename 'Blog publisher'
  #  l.modules [:users, :dashboard, :write, :articles, :pages, :feedback, :media ]
  #end
  #
  #factory :profile_contributor, :parent => :profile do |l|
  #end

  factory :category do |c|
    c.name {FactoryGirl.generate(:category)}
    c.slug {FactoryGirl.generate(:category)}
    c.position 1
  end

  factory :tag do |tag|
    tag.name {FactoryGirl.generate(:name)}
    tag.slug { |a| a.name }
  end

  factory :resource do |r|
    #r.upload {FactoryGirl.generate(:file_name)}
    r.ext 'jpg'
    r.mime 'image/jpeg'
    #r.size 110
  end

  #factory :avatar, parent: :resource do |a|
  #  a.upload "avatar.jpg"
  #  a.mime 'image.jpeg'
  #  a.size 600
  #end

  factory :redirect do |r|
    r.from_path 'foo/bar'
    r.to_path '/someplace/else'
  end

  factory :page do
    name {FactoryGirl.generate(:name)}
    title 'Page One Title'
    body {FactoryGirl.generate(:body)}
    created_at '2005-05-05 01:00:01'
    published_at '2005-05-05 01:00:01'
    updated_at '2005-05-05 01:00:01'
    user
    published true
    state 'published'
  end
end
