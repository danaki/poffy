require 'spec_helper'

describe Admin::DashboardController do
  render_views
    
  describe 'test admin profile' do
    before do
      @blog ||= FactoryGirl.create(:blog)
      @henri = FactoryGirl.create(:user, :login => 'henri', :profile => FactoryGirl.create(:profile_admin, :label => Profile::ADMIN))
      request.session = { :user => @henri.id }
      get :index
    end
    
    it "should render the index template" do
      response.should render_template('index')
    end

    it "should have a link to the theme" do
      response.should have_selector("a", :href => "/admin/themes" , :articles => "change your blog presentation")
    end
    
    it "should have a link to the sidebar" do
      response.should have_selector("a", :href => "/admin/sidebar" , :articles => "enable plugins")
    end
    
    it "should have a link to plugins.publify.co" do
      response.should have_selector("a", :href => "http://plugins.publify.co" , :articles => "download some plugins")
    end
    
    it "should have a link to a new article" do
      response.should have_selector("a", :href => "/admin/content/new" , :articles => "write a post")
    end

    it "should have a link to a new page" do
      response.should have_selector("a", :href => "/admin/pages/new" , :articles => "write a page")
    end
    
    it "should have a link to article listing" do
      response.should have_selector("a", :href => "/admin/content" , :articles => "0 articles")
    end
    
    it "should have a link to user's article listing" do
      response.should have_selector("a", :href => "/admin/content?search%5Buser_id%5D=#{@henri.id}" , :articles => "0 articles writen by you")
    end

    it "should have a link to drafts" do
      response.should have_selector("a", :href => "/admin/content?search%5Bstate%5D=drafts" , :articles => "0 drafts")
    end

    it "should have a link to pages" do
      response.should have_selector("a", :href => "/admin/pages" , :articles => "0 pages")
    end

    it "should have a link to total comments" do
      response.should have_selector("a", :href => "/admin/feedback" , :articles => "0 comments")
    end

    it "should have a link to Spam" do
      response.should have_selector("a", :href => "/admin/feedback?spam=f" , :articles => "0 spam")
    end

    it "should have a link to Spam queue" do
      response.should have_selector("a", :href => "/admin/feedback?presumed_ham=f" , :articles => "0 unconfirmed")
    end
  end
  
  describe 'test publisher profile' do
    before do
      @blog ||= FactoryGirl.create(:blog)
      #TODO Delete after removing fixtures
      Profile.delete_all
      @rene = FactoryGirl.create(:user, :login => 'rene', :profile => FactoryGirl.create(:profile_publisher, :label => Profile::PUBLISHER))
      request.session = { :user => @rene.id }
      get :index
    end
    
    it "should render the index template" do
      response.should render_template('index')
    end

    it "should not have a link to the theme" do
      response.should_not have_selector("a", :href => "/admin/themes" , :articles => "change your blog presentation")
    end
    
    it "should not have a link to the sidebar" do
      response.should_not have_selector("a", :href => "/admin/sidebar" , :articles => "enable plugins")
    end
    
    it "should not have a link to plugins.publify.co" do
      response.should_not have_selector("a", :href => "http://plugins.publify.co" , :articles => "download some plugins")
    end
    
    it "should have a link to a new article" do
      response.should have_selector("a", :href => "/admin/content/new" , :articles => "write a post")
    end

    it "should have a link to a new page" do
      response.should have_selector("a", :href => "/admin/pages/new" , :articles => "write a page")
    end
    
    it "should have a link to article listing" do
      response.should have_selector("a", :href => "/admin/content" , :articles => "0 articles")
    end
    
    it "should have a link to user's article listing" do
      response.should have_selector("a", :href => "/admin/content?search%5Buser_id%5D=#{@rene.id}" , :articles => "0 articles writen by you")
    end

    it "should have a link to total comments" do
      response.should have_selector("a", :href => "/admin/feedback" , :articles => "0 comments")
    end

    it "should have a link to Spam" do
      response.should have_selector("a", :href => "/admin/feedback?spam=f" , :articles => "0 spam")
    end

    it "should have a link to Spam queue" do
      response.should have_selector("a", :href => "/admin/feedback?presumed_ham=f" , :articles => "0 unconfirmed")
    end
  end
  
  describe 'test contributor profile' do
    before do
      @blog ||= FactoryGirl.create(:blog)
      #TODO Delete after removing fixtures
      Profile.delete_all
      @gerard = FactoryGirl.create(:user, :login => 'gerard', :profile => FactoryGirl.create(:profile_contributor, :label => Profile::CONTRIBUTOR))
      request.session = { :user => @gerard.id }
      get :index
    end
    
    it "should render the index template" do
      response.should render_template('index')
    end

    it "should not have a link to the theme" do
      response.should_not have_selector("a", :href => "/admin/themes" , :articles => "change your blog presentation")
    end
    
    it "should not have a link to the sidebar" do
      response.should_not have_selector("a", :href => "/admin/sidebar" , :articles => "enable plugins")
    end
    
    it "should not have a link to plugins.publify.co" do
      response.should_not have_selector("a", :href => "http://plugins.publify.co" , :articles => "download some plugins")
    end
    
    it "should not have a link to a new article" do
      response.should_not have_selector("a", :href => "/admin/content/new" , :articles => "write a post")
    end

    it "should not have a link to a new article" do
      response.should_not have_selector("a", :href => "/admin/pages/new" , :articles => "write a page")
    end
    
    it "should not have a link to article listing" do
      response.should_not have_selector("a", :href => "/admin/content" , :articles => "Total posts:")
    end
    
    it "should not have a link to user's article listing" do
      response.should_not have_selector("a", :href => "/admin/content?search%5Buser_id%5D=#{@gerard.id}" , :articles => "Your posts:")
    end

    it "should not have a link to categories" do
      response.should_not have_selector("a", :href => "/admin/categories" , :articles => "Categories:")
    end

    it "should not have a link to total comments" do
      response.should_not have_selector("a", :href => "/admin/feedback" , :articles => "Total comments:")
    end

    it "should not have a link to Spam" do
      response.should_not have_selector("a", :href => "/admin/feedback?published=f" , :articles => "Spam comments:")
    end

    it "should not have a link to Spam queue" do
      response.should_not have_selector("a", :href => "/admin/feedback?presumed_spam=f" , :articles => "In your spam queue:")
    end
  end
end
