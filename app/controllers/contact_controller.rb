class ContactController < ApplicationController
  def deliver
    c = ContactForm.new(params[:contact_form])

    respond_to do |format|
      if c.deliver
        format.json { render json: { status: :ok, message: I18n.t('mail_form.success') } }
      else
        format.json { render json: { status: :error, errors: c.errors.keys } }
      end
    end
  end
end