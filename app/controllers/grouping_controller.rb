class GroupingController < ContentController
  #layout :theme_layout

  #caches_page :index, :show, :if => Proc.new {|c|
  #  c.request.query_string == ''
  #}

  def self.grouping_class(klass = nil)
    if klass
      @grouping_class = klass
    end
    @grouping_class ||= self.to_s.sub(/Controller$/,'').singularize.constantize
  end

  def self.ivar_name
    @ivar_name ||= "@#{controller_name}"
  end

  #def index
  #  self.groupings = grouping_class.page(params[:page]).per(100)
  #  @page_title = self.controller_name.capitalize
  #  #@keywords = ""
  #  @description = "#{_(self.class.to_s.sub(/Controller$/,''))} #{'for'} #{this_blog.blog_name}"
  #  @description << "#{_('page')} #{params[:page]}" if params[:page]
  #end

  def show
    @grouping = grouping_class.friendly.find(params[:id])

    @canonical_url = permalink_with_page @grouping, params[:page]
    show_page_title_for @grouping, params[:page]

    @meta_title = @grouping.meta_title
    @meta_description = @grouping.meta_description

    # @description = @grouping.description.to_s
    #@keywords = ""
    #
    #@keywords << @grouping.keywords if @grouping.respond_to?(:keywords) && ! @grouping.keywords.blank?
    #@keywords << this_blog.meta_keywords unless this_blog.meta_keywords.blank?

    @articles = @grouping.articles.published.page(params[:page]).per(10)

    render "articles/search_#{grouping_name.singularize.downcase}"
  end

  protected

  def grouping_class
    self.class.grouping_class
  end

  def groupings=(groupings)
    instance_variable_set(self.class.ivar_name, groupings)
  end

  def groupings
    instance_variable_get(self.class.ivar_name)
  end

  def grouping_name
    @grouping_name ||= self.class.to_s.sub(/Controller$/,'')
  end

  def show_page_title_for grouping, page
    if grouping_name.singularize == 'Theme'
      @page_title  = this_blog.theme_title_template.to_title(@grouping, this_blog, params)
      @description = this_blog.theme_desc_template.to_title(@grouping, this_blog, params)
    elsif grouping_name.singularize == 'Tag'
      @page_title  = this_blog.tag_title_template.to_title(@grouping, this_blog, params)
      @description = this_blog.tag_desc_template.to_title(@grouping, this_blog, params)
    end
  end

  # For some reasons, the permalink_url does not take the pagination.
  def permalink_with_page grouping, page
    suffix = page.nil? ? "/" : "/page/#{page}/"
    grouping.permalink_url + suffix
  end
end
