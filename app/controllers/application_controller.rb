# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
#  include ::LoginSystem
  #helper CalendarDateSelect::FormHelpers

  before_filter :configure_permitted_parameters, if: :devise_controller?
  layout "application", if: :devise_controller?
  theme :theme_resolver

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

  protect_from_forgery :only => [:edit, :update, :delete]

  before_filter :reset_local_cache, :fire_triggers, :load_lang, :set_paths
  after_filter :reset_local_cache

  class << self
    unless self.respond_to? :template_root
      def template_root
        ActionController::Base.view_paths.last
      end
    end
  end

  protected

  def set_paths
    Dir.glob(File.join(::Rails.root.to_s, "lib", "*_sidebar/app/views")).select do |file|
      append_view_path file
    end
  end

  def theme_resolver
    Rails.configuration.poffy.theme
  end

  def error(message = "Record not found...", options = { })
    @message = message.to_s
    render 'articles/error', :status => options[:status] || 404
  end

  def fire_triggers
    Trigger.fire
  end

  def load_lang
    lang = Rails.configuration.poffy.lang
    #Localization.lang = lang

    # Check if for example "en_UK" locale exesists if not check for "en" locale
    if I18n.available_locales.include?(lang.to_sym)
      I18n.locale = lang
    elsif I18n.available_locales.include?(lang[0..1].to_sym)
      I18n.locale = lang[0..1]
    end
    # _("Localization.rtl")
  end

  def reset_local_cache
    if !session
      session :session => new
    end
    @current_user = nil
  end

  # The base URL for this request, calculated by looking up the URL for the main
  # blog index page.
  def blog_base_url
    url_for(:controller => '/articles').gsub(%r{/$},'')
  end

  def add_to_cookies(name, value, path=nil, expires=nil)
    cookies[name] = { :value => value, :path => path || "/#{controller_name}", :expires => 6.weeks.from_now }
  end

  def this_blog
    @blog ||= Blog.default
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password) }
  end
end
