class ArticlesController < ContentController
  before_filter :authenticate_user!, :only => [:preview, :preview_page]
  before_filter :verify_config

  helper :'admin/base'

  def index
    conditions = ["type = ?", "Article"]

    #limit = this_blog.per_page(params[:format])
    limit = 2 + 6 + 10 + 2 + 1 + 6

    unless params[:year].blank?
      @articles = Content.published_at(params.values_at(:year, :month, :day)).chronologically.where(conditions).page(params[:page]).per(limit)
    else
      @articles = Content.published.chronologically.where(conditions).page(params[:page]).per(limit)
    end

    if params[:year]
      @page_title = this_blog.archives_title_template
      @description = this_blog.archives_desc_template
      item = @articles
    elsif params[:page]
      @page_title = this_blog.paginated_title_template
      @description = this_blog.paginated_desc_template
      item = @articles
    else
      @page_title = this_blog.home_title_template
      @description = this_blog.home_desc_template
      item = this_blog
    end

    @page_title = @page_title.to_title(item, this_blog, params)
    @description = @description.to_title(item, this_blog, params)

    #@keywords = this_blog.meta_keywords

    #suffix = (params[:page].nil? and params[:year].nil?) ? "" : "/"

    respond_to do |format|
      format.html { render_paginated_index }
      format.atom do
        render_articles_feed('atom')
      end
      format.rss do
        render_articles_feed('rss')
      end
    end
  end

  def search
    @articles = this_blog.articles_matching(params[:q], page: params[:page], per_page: this_blog.per_page(params[:format]) )
    #return error(t("errors.no_posts_found"), :status => 200) if @articles.empty?
    @page_title = this_blog.search_title_template.to_title(@articles, this_blog, params)
    @description = this_blog.search_desc_template.to_title(@articles, this_blog, params)
    @query = params[:q]

    respond_to do |format|
      format.html { render 'search' }
      format.rss { render "index_rss_feed", :layout => false }
      format.atom { render "index_atom_feed", :layout => false }
    end
  end

  def live_search
    @search = params[:q]
    @articles = Article.search(@search)
    render :live_search, :layout => false
  end

  def preview
    @article = Article.last_draft(params[:article_id])
    @page_title = this_blog.article_title_template.to_title(@article, this_blog, params)
    render 'read'
  end

  def redirect
    from = extract_feed_format(params[:from])
    factory = Article::Factory.new(this_blog, current_user)

    @article = factory.match_permalink(from)
    return show_article if @article

    r = Redirect.find_by_from_path(from)
    return redirect_to r.full_to_path, status: 301 if r

    raise ActiveRecord::RecordNotFound
  end

  def archives
    @articles = Article.find_published
    @page_title = this_blog.archives_title_template.to_title(@articles, this_blog, params)
    #@keywords = this_blog.meta_keywords
    @description = this_blog.archives_desc_template.to_title(@articles, this_blog, params)
  end
  #
  #def category
  #  redirect_to categories_path, status: 301
  #end
  #
  #def tag
  #  redirect_to tags_path, status: 301
  #end

  def preview_page
    @page = Page.find(params[:id])
    render 'view_page'
  end

  def view_page
    if (@page = Page.friendly.find(params[:id])) && @page.published?
      @page_title = @page.title
      @description = this_blog.description
      #@keywords = this_blog.meta_keywords
    else
      render "errors/404", status: 404
    end
  end

  def recent
    @recent = Article.published.chronologically.page(params[:page]).per(8)

    respond_to do |format|
      # format.html
      format.js
    end
  end

  private

  def verify_config
    if User.count == 0
      redirect_to new_user_session_path
      #redirect_to controller: "accounts", action: "signup"
    else
      return true
    end
  end

  # See an article We need define @article before
  def show_article
    if ! current_user.present? then
      @article.update_attribute(:view_count, @article.view_count + 1)
    end

    @page_title  = this_blog.article_title_template.to_title(@article, this_blog, params)
    # @description = this_blog.article_desc_template.to_title(@article, this_blog, params)
    groupings = @article.themes + @article.tags
    @keywords = groupings.map { |g| g.title }.join(", ")

    @meta_title = @article.meta_title
    @meta_description = @article.meta_description

    @recent_articles = Article.published.chronologically.page(1).per(8)

    render "articles/#{@article.post_type}"
  rescue ActiveRecord::RecordNotFound
    error("Post not found...")
  end

  def render_articles_feed format
    if this_blog.feedburner_url.empty? or request.env["HTTP_USER_AGENT"] =~ /FeedBurner/i
      render "index_#{format}_feed", :layout => false
    else
      redirect_to "http://feeds2.feedburner.com/#{this_blog.feedburner_url}"
    end
  end

  def render_paginated_index(on_empty = t("errors.no_posts_found"))
    return error(on_empty, :status => 200) if @articles.empty?

    render params[:page].nil? || params[:page].to_i < 2 ? 'index' : 'search'
  end

  def extract_feed_format(from)
    if from =~ /^.*\.rss$/
      request.format = 'rss'
      from = from.gsub(/\.rss/,'')
    elsif from =~ /^.*\.atom$/
      request.format = 'atom'
      from = from.gsub(/\.atom$/,'')
    end
    from
  end

end
