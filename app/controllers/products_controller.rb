class ProductsController < ContentController

  def index
    @product_category = ProductCategory.friendly.find(params[:id])

    if @product_category.nil?
      render "errors/404", status: 404
    else
      parent_products = this_blog.product_model
        .joins(:product_categories)
        .where('categories.id' => @product_category.id)
        .order("contents.settings -> 'prio' ASC").published.all

      child_products = this_blog.product_model
        .joins(:product_categories)
        .where('categories.id' => @product_category.children.collect(&:id))
        .order("contents.settings -> 'prio' ASC").published.all

      @products = parent_products + child_products

      @subcategories = if @product_category.root.nil?
                         @product_category.children
                       else
                         @product_category.root.children
                       end

      @page_title = this_blog.products_list_title_template
      @description = this_blog.products_list_desc_template

      @page_title = @page_title.to_title(@product_category, this_blog, params)
      @description = @description.to_title(@product_category, this_blog, params)
    end
  end

  def show
    if (@product = this_blog.product_model.friendly.find(params[:id])) && @product.published?
      @page_title = this_blog.product_title_template
      # @description = this_blog.products_list_desc_template

      @page_title = @page_title.to_title(@product, this_blog, params)

      @meta_description = @product.meta_description

      # @description = @description.to_title(@product, this_blog, params)
    else
      render "errors/404", status: 404
    end

  end

end