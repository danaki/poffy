class Admin::TagsController < Admin::BaseController
  include TheSortableTreeController::Rebuild

  before_filter :load

  def index; redirect_to action: 'new'; end
  def show; redirect_to action: 'edit', id: params[:id]; end

  def new
    #@tag = Tag.new
  end

  def create
    #@tag = Tag.new(resource_params)

    if @tag.save
      flash[:notice] = "Tag successfully created"
      redirect_to admin_tags_path
    else
      render :action => 'new'
    end
  end

  def edit
    #@tag = Tag.find(params[:id])
    render :action => 'new'
  end

  def update
    #@tag = Tag.find(params[:id])

    if @tag.update(resource_params)
      flash[:notice] = "Tag successfully updated"
      redirect_to admin_tags_path
    else
      render :action => 'new'
    end
  end

  def destroy
    destroy_a(Tag)
  end

  private

    def load
      @tags = Tag.nested_set.all
    end

    def resource_params
      params.require(:tag).permit(
        :title,
        :parent_id,
        :slug,
        :body,
        :description,
        *Tag.fields.keys
      )
    end
end
