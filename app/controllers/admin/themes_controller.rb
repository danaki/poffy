class Admin::ThemesController < Admin::BaseController
  include TheSortableTreeController::Rebuild

  before_filter :load

  def index; redirect_to action: 'new'; end
  def show; redirect_to action: 'edit', id: params[:id]; end

  def new
    #@theme = Theme.new
  end

  def create
    #@Theme = Theme.new(post_params)

    if @theme.save
      flash[:notice] = "Theme successfully created"
      redirect_to admin_themes_path
    else
      render :action => 'new'
    end
  end

  def edit
    #@theme = Theme.find(params[:id])
    render :action => 'new'
  end

  def update
    if @theme.update(resource_params)
      flash[:notice] = "Theme successfully updated"
      redirect_to admin_themes_path
    else
      render :action => 'new'
    end
  end

  def destroy
    destroy_a(Theme)
  end

  private

    def load
      @themes = Theme.nested_set.all
    end

    def resource_params
      params.require(:theme).permit(
        :title,
        :parent_id,
        :slug,
        :body,
        *Theme.fields.keys
      )
    end
end
