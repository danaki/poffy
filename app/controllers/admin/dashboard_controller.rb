class Admin::DashboardController < Admin::BaseController
  skip_load_and_authorize_resource

  def index
    authorize! :index, :dashboard

    t = Time.new
    today = t.strftime("%Y-%m-%d 00:00")

    # Since last venue
    @newposts_count = Article.published_since(current_user.last_venue).count

    # Today
    @statposts = Article.published.where("published_at > ?", today).count
    @statsdrafts = Article.drafts.where("created_at > ?", today).count
    @statspages = Page.where("published_at > ?", today).count
    @statuserposts = Article.published.where("published_at > ?", today).where(user_id: current_user.id).count
    @drafts = Article.drafts.where("user_id = ?", current_user.id).limit(5)
  end

end
