# coding: utf-8
require 'base64'

class Admin::PagesController < Admin::BaseController
  skip_load_and_authorize_resource

  layout "administration", :except => 'show'

  def index
    @search = params[:search] ? params[:search] : {}
    @pages = Page.search_with(@search).page(params[:page]).per(this_blog.admin_display_elements)
  end

  def new
    @page = Page.new(params[:page])
    @page.user_id = current_user.id
  end

  def edit
    @page = Page.find(params[:id])
  end

  def create
    @page = Page.new(resource_params)
    @page.user_id = current_user.id

    @page.published_at = Time.now

    if @page.save
      flash[:notice] = 'Page was successfully created.'
      redirect_to :action => 'index'
    else
      flash[:notice] = @page.errors.full_messages.to_sentence
      render 'new'
    end
  end

  def update
    @page = Page.find(params[:id])
    @page.attributes = resource_params
    #@page.text_filter ||= default_textfilter

    if @page.save
      flash[:notice] = 'Page was successfully updated.'
      redirect_to :action => 'index'
    else
      flash[:notice] = @page.errors.full_messages.to_sentence
      render 'edit'
    end
  end

  def destroy
    destroy_a(Page)
  end

  private

  def resource_params
    params.require(:page).permit(
      :title,
      :slug,
      :body,
      :published,
      *Page.fields_for_merged_self.keys,
      :_removed_images => [],
      :_prev_images => []
    )
  end

end
