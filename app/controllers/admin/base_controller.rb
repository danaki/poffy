class Admin::BaseController < ApplicationController

  load_and_authorize_resource

  #cattr_accessor :look_for_migrations

  #@@look_for_migrations = true
  layout 'administration'

  #before_filter :login_required, :except => [ :login, :signup ]
  #before_filter :look_for_needed_db_updates, :except => [:login, :signup, :update_database, :migrate]
  before_filter :set_english_locale
  before_action :authenticate_user!, :except => [ :login, :signup ]

  private

  def set_english_locale
    I18n.locale = :en
  end

  def parse_date_time(str)
    begin
      DateTime.strptime(str, "%B %e, %Y %I:%M %p GMT%z").utc
    rescue
      Time.parse(str).utc rescue nil
    end
  end

  def save_a(object, title)
    if object.save
      flash[:notice] = "#{title.capitalize} was successfully saved."
    else
      flash[:error] = "#{title.capitalize} could not be saved."
    end
    redirect_to action: 'index'
  end

  def destroy_a(klass_to_destroy)
    @record = klass_to_destroy.find(params[:id])
    if @record.respond_to?(:access_by?) && !@record.access_by?(current_user)
      flash[:error] = _("Error, you are not allowed to perform this action")
      return(redirect_to action: 'index')
    end

    @record.destroy
    flash[:notice] = "This #{controller_name.humanize} was deleted successfully"
    redirect_to action: 'index'
  end

  #def all_models
  #  Module.constants.select do |constant_name|
  #    constant = eval constant_name.to_s
  #    if not constant.nil? and constant.is_a? Class and constant.extend? ActiveRecord::Base
  #      constant
  #    end
  #  end
  #end
  #
  #def models_with_slug
  #  all_models.select { |i| Module.const_get(i).column_names.include? "slug" }
  #end
  #
  #def find_models_by_slug(slug)
  #  results = []
  #  models_with_slug.each do |m|
  #    item = m.find(slug: slug)
  #    unless item.null?
  #      results << item
  #    end
  #  end
  #
  #  results
  #end

end
