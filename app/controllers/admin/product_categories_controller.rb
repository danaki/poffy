class Admin::ProductCategoriesController < Admin::BaseController
  include TheSortableTreeController::Rebuild

  before_filter :load

  def index; redirect_to action: 'new'; end
  def show; redirect_to action: 'edit', id: params[:id]; end

  def new
    #@product_category = ProductCategory.new
  end

  def create
    #@Theme = Theme.new(post_params)

    if @product_category.save
      flash[:notice] = "Product category successfully created"
      redirect_to admin_product_categories_path
    else
      render :action => 'new'
    end
  end

  def edit
    #@product_category = ProductCategory.find(params[:id])
    render :action => 'new'
  end

  def update
    if @product_category.update(resource_params)
      flash[:notice] = "Product category successfully updated"
      redirect_to admin_product_categories_path
    else
      render :action => 'new'
    end
  end

  def destroy
    destroy_a(ProductCategory)
  end

  private

  def load
    @product_categories = ProductCategory.nested_set.all
  end

  def resource_params
    params.require(:product_category).permit(
      :title,
      :parent_id,
      :slug,
      :body,
      :description,
      *ProductCategory.fields.keys
    )
  end
end
