class Admin::UsersController < Admin::BaseController

  skip_load_and_authorize_resource

  def index
    @users = User.order('username asc').page(params[:page]).per(this_blog.admin_display_elements)
  end

  def new
    @user = User.new
    @user.name = @user.login
  end

  def create
    confirmed = params[:user][:confirmed]

    params = resource_params
    @user = User.new(params)

    if confirmed && current_user.admin?
      @user.skip_confirmation!
      @user.confirm!
    end

    if @user.save
      flash[:notice] = 'User was successfully created.'
      redirect_to :action => 'index'
    else
      render 'new'
    end
  end

  def edit
    @user = params[:id] ? User.find_by_id(params[:id]) : current_user
  end

  def update
    @user = User.find(params[:id])

    confirmed = params[:user][:confirmed]
    params = resource_params

    updated = if current_user.admin?
      if params[:password].blank?
        params.delete(:password)
        params.delete(:password_confirmation) if params[:password_confirmation].blank?
      end

      if confirmed
        @user.confirm!
      end

      @user.skip_reconfirmation!

      @user.update_attributes(params)
    else
      if params[:password].present?
        @user.update_with_password(params)
      else
        params.delete(:current_password)
        @user.update_without_password(params)
      end
    end

    @user.clean_up_passwords

    if updated
      if @user.id = current_user.id
        current_user = @user
      end

      flash[:notice] = 'User was successfully updated.'
      redirect_to admin_users_path
    else
      render 'edit'
    end
  end

  def destroy
    @record = User.find(params[:id])
    return(render 'admin/shared/destroy') unless request.post?

    @record.destroy if User.count > 1
    redirect_to :action => 'index'
  end

  private

  def resource_params
    keys = [
      :username,
      :name,
      :url,
      :description,
      :password,
      :password_confirmation,
      :email,
      :notify_via_email,
      :notify_on_new_articles
    ]

    keys += if current_user.admin?
              [:state, :role_ids => []]
            else
              [:current_password]
            end

    params.require(:user).permit(*keys)
  end
end
