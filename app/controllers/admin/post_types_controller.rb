class Admin::PostTypesController < Admin::BaseController
  before_filter :load

  def index; redirect_to action: 'new'; end
  def show; redirect_to action: 'edit', id: params[:id]; end

  def new
    #@post_type = PostType.new
  end

  def create
    if @post_type.save
      flash[:notice] = "PostType successfully created"
      redirect_to admin_post_types_path
    else
      render :action => 'new'
    end
  end

  def edit
    #@post_type = PostType.find(params[:id])
    render :action => 'new'
  end

  def update
    if @post_type.update(resource_params)
      flash[:notice] = "PostType successfully updated"
      redirect_to admin_post_types_path
    else
      render :action => 'new'
    end
  end

  def destroy
    @record = PostType.find(params[:id])

    #Content.where("post_type = ?", @record.slug).each do |article|
    #  article.post_type = 'read'
    #  article.save
    #end
    @record.destroy

    redirect_to :action => 'new'
  end

  private

    def load
      @post_types = PostType.all
    end

    def resource_params
      params.require(:post_type).permit(
        :title,
        :slug,
        :description
      )
    end

end
