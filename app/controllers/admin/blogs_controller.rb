class Admin::BlogsController < Admin::BaseController
  skip_load_and_authorize_resource

  def index
    if this_blog.base_url.blank?
      this_blog.base_url = blog_base_url
    end
    load_settings
  end

  def integration; load_settings end
  def show; load_settings end
  def display; load_settings end
  def titles; load_settings end

  def seo
    load_settings
    @blog.robots = Robot.new.rules
  end

  def slugs
    @items = []
    Validators::GlobalSlugValidator.models_with_slug.each do |m|
      @items = @items.concat(Module.const_get(m).where.not(slug: nil).all)
    end
  end

  #def redirect
  #  flash[:notice] = _("Please review and save the settings before continuing")
  #  redirect_to :action => "index"
  #end

  def update
    update_settings_with!(params)
    if params[:blog][:robots].present?
      Robot.new.add(params[:blog][:robots])
    end

    redirect_to action: params[:from]
  end

  private

  def load_settings
    @blog = this_blog
  end

  def update_settings_with!(params)
    Blog.transaction do
      params[:blog].each { |k,v| this_blog.send("#{k.to_s}=", v) }
      this_blog.save
      flash[:notice] = 'Config updated'
    end
  end
end
