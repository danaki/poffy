#require 'fog'

class Admin::ResourcesController < Admin::BaseController

  def index
    @r = Resource.new
    @resources = Resource.order('created_at DESC').page(params[:page]).per(this_blog.admin_display_elements)
  end

  def upload
    file = params[:upload][:filename]
    kind = params[:upload][:kind]
    constraints = Resource.thumbnails_for(kind)[:full]
    _, @w, @h = constraints.match(/^(.*)x(.*)$/).to_a

    @up = ResourceUploader.new(Resource.original_dir)
    @up.store!(file)

    #@file_data = uploader.file.read()

    respond_to do |format|
      format.html { redirect_to :action => "index" }
      format.js
    end
  end

  def create
    original = params[:preview][:original_image]

    kind   = params[:preview][:kind]
    crop_x = params[:preview][:crop_x]
    crop_y = params[:preview][:crop_y]
    crop_w = params[:preview][:crop_w]
    crop_h = params[:preview][:crop_h]

    @resource = Resource.new
    @resource.ext = File.extname(original).downcase

    @resource.save

    @resource.crop(
        original,
        Resource.thumbnails_for(kind),
        crop_x,
        crop_y,
        crop_w,
        crop_h
    )

    respond_to do |format|
      format.html { redirect_to :action => "index" }
      format.js
    end
  end

  def wysiwyg_upload
    file = params[:file]

    original = file.original_filename
    filename  = File.basename(original, ".*").to_slug.normalize(transliterate: Rails.configuration.poffy.transliteration.to_sym).to_s
    extname = File.extname(original)
    file.original_filename = "#{filename}-#{(Time.now.to_i.to_s)}#{extname}"

    @uploader = WysiwygUploader.new("files/wysiwyg/post")
    @uploader.store!(file)

    render json: {
      error: false,
      path: @uploader.url
    }, content_type: "text/html"
  end

  def get_thumbnails
    position = params[:position].to_i
    @resources = Resource.without_images.by_created_at.limit(10).offset(position)

    render 'get_thumbnails', :layout => false
  end

  def destroy
    begin
      @record = Resource.find(params[:id])
      mime = @record.mime

      @record.destroy
      redirect_to :action => 'index'
    rescue
      raise
    end
  end

end
