class Admin::MenusController < Admin::BaseController
  include TheSortableTreeController::Rebuild

  before_filter :load

  def index; redirect_to action: 'new'; end
  def show; redirect_to action: 'edit', id: params[:id]; end
  def new; end

  def create
    #@menu = Menu.new(post_params)

    if @menu.save
      flash[:notice] = "Menu successfully created"
      redirect_to admin_menus_path
    else
      render :action => 'new'
    end
  end

  def edit
    #@menu = Menu.find(params[:id])
    render :action => 'new'
  end

  def update
    #@menu = Menu.find(params[:id])

    if @menu.update(resource_params)
      flash[:notice] = "Menu successfully updated"
      redirect_to admin_menus_path
    else
      render :action => 'new'
    end
  end

  def destroy
    destroy_a(Menu)
  end

  private

  def load
    @menus = Menu.nested_set.all
  end

  def resource_params
    params.require(:menu).permit(
      :title,
      :link,
      :parent_id
    )
  end
end
