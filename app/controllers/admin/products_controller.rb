
class Admin::ProductsController < Admin::BaseController
  skip_load_and_authorize_resource

  layout "administration", :except => 'show'

  def index
    @search = params[:search] ? params[:search] : {}
    @products = this_blog.product_model
      .search_with(@search)
      .page(params[:page])
      .per(this_blog.admin_display_elements)
  end

  def new
    @product = this_blog.product_model.new(params[:product])
    @product.user_id = current_user.id
  end

  def edit
    @product = this_blog.product_model.find(params[:id])
  end

  def create
    @product = this_blog.product_model.new(resource_params)
    @product.user_id = current_user.id

    @product.published_at = Time.now

    if @product.save
      flash[:notice] = 'Product was successfully created.'
      redirect_to :action => 'index'
    else
      flash[:notice] = @product.errors.full_messages.to_sentence
      render 'new'
    end
  end

  def update
    @product = this_blog.product_model.find(params[:id])
    @product.attributes = resource_params
    if @product.save
      flash[:notice] = 'Product was successfully updated.'
      redirect_to :action => 'index'
    else
      flash[:notice] = @product.errors.full_messages.to_sentence
      render 'edit'
    end
  end

  def destroy
    destroy_a(this_blog.product_model)
  end

  private

  def resource_params
    params.require(this_blog.product_model.name.underscore).permit(
      :title,
      :slug,
      :body,
      :description,
      :resource_id,
      :published,
      *this_blog.product_model.fields_for_merged_self.keys,
      :product_category_ids => [],
      :_removed_images => [],
      :_prev_images => []
    )
  end

end
