class DisqusDiscussionsSidebarCell < DiscussionsSidebarCell
  inherit_views DiscussionsSidebarCell

  include ApplicationHelper
  include DisqusHelper

  def count
    13
  end

  def show
    render
  end

  private

  def recently_published
    Article.recent.page(1).per(count)
  end

  def recently_commented
    if disqus_app_configured? then
      ids = disqus_recent_post_ids(count)
      ids.count > 0 ? Article.where(id: ids).take(count).to_a.sort_by { |a| ids.index(a.id.to_s) } : []
    else
      nil
    end
  end

  def disqus_recent_post_ids(count)
    ids = []
    shortname = this_blog.disqus_shortname
    key = this_blog.disqus_api_key
    begin
      contents = Rails.cache.fetch("disqus_recent_posts", expires_in: 1.minute) do
        url = "http://disqus.com/api/3.0/posts/list.json?api_key=#{key}&forum=#{shortname}&limit=25&related=thread"
        open(url).read
      end

      json_object = JSON.parse(contents)
      if json_object['code'] == 0 then
        json_object['response'].each do |item|
          ids << item['thread']['identifiers'][0]
        end
      end

      ids.uniq!
    rescue
    end

    ids
  end

end
