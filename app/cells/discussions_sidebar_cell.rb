class DiscussionsSidebarCell < Cell::ViewModel
  private

  def sidebar_article(article, from)
    link = article.permalink_url
    title = article.title
    id = article.id
    comments = (disqus_configured? and (not id.nil?) and (not id.to_s.empty?))

    <<-HTML
      <article class="post">
        <a href="#{ link }">#{ title }</a>
        <a #{ if comments then 'class="po-icons comments"' end } href="#{ link }#{ if comments then '#disqus_thread" data-disqus-identifier="' + id.to_s end }"> </a>
      </article>
    HTML
  end

end
