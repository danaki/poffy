class ProductRelatedPostsSidebarCell < Cell::ViewModel
  inherit_views PostsSidebarCell

  include ArticlesHelper
  include ThemesForRails::UrlHelpers
  include ActionView::Helpers::AssetTagHelper

  def show
    render
  end

  private

  def articles
    model.related_articles.take(3)
  end

  def title
    I18n.t("sidebar.related.title")
  end

  def from
    'related'
  end

end
