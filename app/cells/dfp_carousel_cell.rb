class DfpCarouselCell < Cell::ViewModel

  include ApplicationHelper

  def show
    render
  end

  private

  def banner_ids
    dfp_list = this_blog.carousel_banners_list.presence

    if dfp_list then
      YAML::load(dfp_list)
    else
      []
    end
  end
end
