class SimilarPostsSidebarCell < Cell::ViewModel
  inherit_views PostsSidebarCell

  include ArticlesHelper
  include ThemesForRails::UrlHelpers
  include ActionView::Helpers::AssetTagHelper

  def show
    render
  end

  private

  def articles
    Article
      .joins(:tags)
      .where('categories.id' => model.tags)
      .where.not(id: model)
      .order('published_at DESC')
      .group('contents.id')
      .take(4)
  end

  def title
    I18n.t("sidebar.similar.title")
  end

  def from
    'similar'
  end

end
