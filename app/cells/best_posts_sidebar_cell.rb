class BestPostsSidebarCell < Cell::ViewModel
  inherit_views PostsSidebarCell

  include ArticlesHelper
  include ThemesForRails::UrlHelpers
  include ActionView::Helpers::AssetTagHelper

  def theme_name
    Rails.configuration.poffy.theme
  end

  def show
    render
  end

  private

  def articles
    week = Article.bestof_week.by_popularity.take(3)
    if week.count == 0
      Article.bestof_month.by_popularity.take(3)
    else
      week
    end
  end

  def title
    I18n.t("sidebar.best.title")
  end

  def from
    'best'
  end

end
