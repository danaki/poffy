/*global tinymce:true */

tinymce.PluginManager.add('po_video', function(editor, url) {

    var def_width = 690;
    var def_height = 410;

    var urlPatterns = [
        {regex: /youtu\.be\/([a-z1-9.-_]+)/, type: 'iframe', w: def_width, h: def_height, url: 'http://www.youtube.com/embed/$1'},
        {regex: /youtube\.com(.+)v=([^&]+)/, type: 'iframe', w: def_width, h: def_height, url: 'http://www.youtube.com/embed/$2'},
        {regex: /vimeo\.com\/([0-9]+)/, type: 'iframe', w: def_width, h: def_height, url: 'http://player.vimeo.com/video/$1?title=0&byline=0&portrait=0&color=8dc7dc'}
    ];

    function showDialog()
    {
        data = getData(editor.selection.getNode());
        width = data.width || def_width;
        height = data.height || def_height;

        editor.windowManager.open({
            title: 'Insert video',
            data: data,
            width: 576,
            height: 182,
            bodyType: 'tabpanel',
            body: [
                {
                    title: 'General',
                    type: "form",
                    onShowTab: function() {
                        this.fromJSON(htmlToData(this.next().find('#embed').value()));
                    },
                    items: [
                        { name: 'source', type: 'textbox', size: 67, autofocus: true, label: 'Ссылка' }
                    ]
                },
                {
                    title: 'Embed',
                    type: "panel",
                    layout: 'flex',
                    direction: 'column',
                    align: 'stretch',
                    padding: 10,
                    spacing: 10,
                    onShowTab: function() {
                        this.find('#embed').value(dataToHtml(this.parent().toJSON()));
                    },
                    items: [
                        {
                            type: 'label',
                            text: 'Paste your embed code below:'
                        },
                        {
                            type: 'textbox',
                            flex: 1,
                            minWidth: 555,
                            minHeight: 104,
                            name: 'embed',
                            value: getSource(),
                            multiline: true,
                            label: 'Source'
                        }
                    ]
                }
            ],
            onSubmit: function() {
                editor.insertContent(dataToHtml(this.toJSON()));
            }
        });
    }

    function getSource() {
        var elm = $(editor.selection.getNode());
        if (elm.data('mce-object') == 'iframe') {
            return editor.selection.getContent();
        }

        return '';
    }

    function getData(elm) {
        if ($(elm).data('mce-object') == 'iframe') {
            return htmlToData(editor.serializer.serialize(elm, {selection: true}));
        }

        return {};
    }

    function dataToHtml(data) {
        var html = '';

        if (! data.source) {
            tinymce.extend(data, htmlToData(data.embed));
            if (!data.source) {
                return '';
            }
        }

        data.source = editor.convertURL(data.source, "source");

        data.width = data.width || width;
        data.height = data.height || height;
        data.class = data.class || height;

        if (data.embed) {
            html = updateHtml(data.embed, data, true);
        } else {
            tinymce.each(urlPatterns, function(pattern) {
                var match, i, url;

                if ((match = pattern.regex.exec(data.source))) {
                    url = pattern.url;

                    for (i = 0; match[i]; i++) {
                        /*jshint loopfunc:true*/
                        url = url.replace('$' + i, function() {
                            return match[i];
                        });
                    }

                    data.source = url;
                    data.type = pattern.type;
                    data.width = pattern.w;
                    data.height = pattern.h;
                }
            });

            tinymce.each(data, function(value, key) {
                data[key] = editor.dom.encode(value);
            });

            html += '<iframe' + (data.class ? ' class"' + data.class + '"' : '') + ' data-src="' + data.source + '" src="' + data.source + '" width="' + data.width + '" height="' + data.height + '"></iframe>';
        }

        return html;
    }

    function htmlToData(html) {
        var data = {};

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {
                if (!data.source && name == "param") {
                    data.source = attrs.map.movie;
                }

                if (name == "iframe") {
                    data = tinymce.extend(attrs.map, data);
                }

                if (name == "source") {
                    if (! data.source) {
                        data.source = attrs.map.src;
                    }
                }
            }
        }).parse(html);

        data.source = data.source || data.src || data.data;

        return data;
    }

    function updateHtml(html, data, updateAll) {
        var writer = new tinymce.html.Writer();
        function setAttributes(attrs, updatedAttrs) {
            var name, i, value, attr;

            for (name in updatedAttrs) {
                value = "" + updatedAttrs[name];

                if (attrs.map[name]) {
                    i = attrs.length;
                    while (i--) {
                        attr = attrs[i];

                        if (attr.name == name) {
                            if (value) {
                                attrs.map[name] = value;
                                attr.value = value;
                            } else {
                                delete attrs.map[name];
                                attrs.splice(i, 1);
                            }
                        }
                    }
                } else if (value) {
                    attrs.push({
                        name: name,
                        value: value
                    });

                    attrs.map[name] = value;
                }
            }
        }

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',

            comment: function(text) {
                writer.comment(text);
            },

            cdata: function(text) {
                writer.cdata(text);
            },

            text: function(text, raw) {
                writer.text(text, raw);
            },

            start: function(name, attrs, empty) {
                setAttributes(attrs, { width: data.width, height: data.height, class: data.class });

                if (updateAll) {
                    setAttributes(attrs, { src: data.source });
                }

                writer.start(name, attrs, empty);
            },

            end: function(name) {
                writer.end(name);
            }
        }, new tinymce.html.Schema({})).parse(html);

        return writer.getContent();
    }

    editor.addButton('po_video', {
        tooltip: 'Insert video',
        icon: 'media',
        onclick: showDialog,
        stateSelector: 'img[data-mce-object=iframe]'
    });

});