/*global tinymce:true */


(function(tinymce) {

    function addCommand(editor, url, headingNumber) {
        var headingTag = 'h' + headingNumber;

        editor.addButton(headingTag, {
            icon: false,
            title : 'Заголовок ' + (headingNumber - 3),
            image : url + '/img/h' + (headingNumber - 3) + '.gif',
            cmd: 'mceHeading' + headingNumber,
            onPostRender: toggleState
        });

        editor.on('keydown', function(e){
            if (e.keyCode == 13) {
                var currNode = editor.selection.getNode();
                if (currNode.nodeName.toLowerCase() == headingTag) {
                    var nextNode = editor.dom.getNext(currNode, '*');
                    if (nextNode) {
                        editor.selection.setCursorLocation(nextNode, 0);
                    }
                    return false;
                }
            }
        });

        editor.addCommand('mceHeading' + headingNumber, function() {
            var sel = editor.selection;
            if (sel.getNode().nodeName.toLowerCase() != headingTag) {
                var ct = sel.getContent();
                if (ct.length) {
                    editor.execCommand('mceInsertContent', false, '<' + headingTag + '>' + ct + '</' + headingTag + '>');
                    var currNode = editor.selection.getNode();
                    if (currNode) {
                        var nextNode = tinymce.activeEditor.dom.getNext(currNode, '*');
                        if (nextNode) {
                            var $br = $(nextNode.firstChild);
                            if ($br && $br[0].nodeType !== 3 && $br[0].tagName.toLowerCase() == 'br') {
                                $br.remove()
                            }
                        }
                    }
                } else {
                    editor.execCommand('FormatBlock', false, headingTag);
                }
            } else {
                editor.execCommand('FormatBlock', false, '');
            }
        });

        editor.on('NodeChange', function(e) {
            editor.fire(headingTag, {state: e.element.nodeName.toLowerCase() == headingTag});
        });

        function toggleState() {
            var self = this;
            editor.on(headingTag, function(e) {
                self.active(e.state);
            });
        }
    }

    tinymce.create('tinymce.plugins.po_heading', {
        init : function(editor, url) {
            for (var headingNumber = 4; headingNumber <= 6; headingNumber++) {
                addCommand(editor, url, headingNumber);
            }
        }
    });

    tinymce.PluginManager.add('po_heading', tinymce.plugins.po_heading);

})(tinymce);