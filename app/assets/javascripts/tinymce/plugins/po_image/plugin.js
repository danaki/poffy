/*global tinymce:true */

tinymce.PluginManager.add('po_image', function(editor, url)
{
    var dialog, uploader, preview, progress, error;
    var form_preview, form_image_url;
    var width, height;

    function showDialog(form)
    {
        dialog = editor.windowManager.open({
            title: 'Insert image',
            type: 'form',
            body: [
                { name: 'preview', type: 'container', layout: 'flex', hidden: true },
                { type: 'container', label: 'Загрузить изображение', layout: 'flex', items: [
                    { name: 'uploader', type: 'button', text: 'Выбрать файл', minWidth: 150 },
                    { type: 'container', minWidth: 150, html: '<div id="uploader-progress"><span></span></div><div id="uploader-error"><span></span></div>' }
                ]},
                { name: 'url', label: 'Ссылка на изображение', type: 'textbox', disabled: true },
                { name: 'alt', type: 'textbox', label: 'Image description', autofocus: true }
            ],
            minWidth: 740,
            onsubmit: onSubmitForm
        })
            .on('onresize', function() {
                dialog.reflow();
                dialog.resizeToContent();
                var box = dialog.initLayoutRect();
                dialog.moveTo(box.x, box.y);
            })
            .on('onerror', function(message) {
                uploader.swfupload('setButtonDisabled', false);
                progress.hide();
                error.show().html(message);
            })
            .on('onfileselected', function() {
                form_preview.hide();
                error.hide();
                dialog.fire('onresize');
                uploader.swfupload('setButtonDisabled', true);
            })
            .on('onprogress', function(value) {
                error.hide();
                progress.show().find("span").css({ width: value });
            });

        form_preview = dialog.find("#preview").hide();
        form_image_url = dialog.find("#url");
        progress = $("#uploader-progress").hide();
        error = $("#uploader-error").hide();

        dialog.find("#uploader")[0].innerHtml('<div id="flash-uploader">Выбрать файл<div id="uploader-button"></div></div>');

        var data = getData(editor.selection.getNode());
        if (data.src) {
            showImage(data.src);
        }
        if (data.alt) {
            dialog.find("#alt").value($('<div />').html(data.alt).text());
        }
        if (editor.settings.image_by_url) {
            form_image_url
                .disabled(false)
                .on('change', function() {
                    showImage(form_image_url.value());
                });
            if (data.src) {
                form_image_url.value(data.src);
            }
        }

        width = data.width || false;
        height = data.height || false;

        dialog.fire('onresize');
    }

    function onSubmitForm(e)
    {
        editor.insertContent(dataToHtml({
            source: $("#uploader-preview").attr('src'),
            alt: e.data.alt.replace(/"/g, "&quot;"),
            width: width,
            height: height
        }));
    }

    function getData(element) {
        if (element.getAttribute('data-mce-src')) {
            return htmlToData(editor.serializer.serialize(element, {selection: true}));
        }

        return {};
    }

    function htmlToData(html)
    {
        var data = {};

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {
                if (name == "img") {
                    data = tinymce.extend(attrs.map, data);
                }
            }
        }).parse(html);
        return data;
    }

    function dataToHtml(data)
    {
        var html = '';
        if (data.source) {
            html = '<img src="' + data.source + '"' + (data.alt ? ' alt="' + data.alt + '"' : '') + (data.width ? ' width="' + data.width + '"' : '') + (data.height ? ' height="' + data.height + '"' : '') + ' />';
        }
        return html;
    }

    function showImage(src)
    {
        var image = new tinymce.ui.Container({minWidth: 698, html: '<p style="text-align: center;"><img style="text-align: center;" id="uploader-preview" src="' + src + '" /></p>'});
        if (form_preview[0].items().length) {
            form_preview[0].items().remove();
        }
        form_preview.append(image);

        $("#uploader-preview").load(function() {
            form_preview.show();
            if ($(this).height() > 430) {
                $(this).height(430);
            }
            image.minHeight = $(this).height();
            form_preview.reflow();
            $(this).hide().fadeIn(500);
            dialog.fire('onresize');
        });
    }

    function initFlashUploader()
    {
        uploader = $("#flash-uploader").swfupload({
            upload_url: editor.settings.image_upload_path,
            file_post_name : 'image_file',
            file_size_limit : 20000000,
            file_types : "*.jpg;*.jpeg;*.gif;*.png;",
            file_types_description : "All Files",
            file_upload_limit : 10,
            flash_url : editor.settings.image_swfupload,
            button_image_url : url + '/img/1.gif',
            button_placeholder_id : "uploader-button",
            button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT,
            button_cursor : SWFUpload.CURSOR.HAND
        })
            .bind('fileQueued', function(event, file){
                dialog.fire('onfileselected');
                uploader.swfupload('startUpload');
            })
            .bind('uploadStart', function(event, file) {
                dialog.fire('onprogress', false, 0);
            })
            .bind('uploadProgress', function(event, file, bytesLoaded){
                if (file.size > 0) {
                    dialog.fire('onprogress', false, (bytesLoaded / file.size * 100));
                }
            })
            .bind('uploadSuccess', function(event, file, serverData){
                uploader.swfupload('setButtonDisabled', false);
                progress.hide();
                data = tinymce.util.JSON.parse(serverData);
                if (data.handshake) {
                    showImage(data.data);
                } else {
                    dialog.fire('onerror', false, data.data);
                }
            })
            .bind('uploadError', function(event, file, errorCode, message){
                dialog.fire('onerror', false, message);
            });
    }

    editor.addButton('po_image', {
        tooltip: 'Insert image',
        icon: 'image',
        onclick: function() {
            showDialog();
            initFlashUploader();
        },
        stateSelector: 'img:not([data-mce-object])'
    });

});