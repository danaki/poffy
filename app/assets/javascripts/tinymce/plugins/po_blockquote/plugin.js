/*global tinymce:true */

tinymce.PluginManager.add('po_blockquote', function(editor, url) {

    editor.addButton('po_blockquote', {
        icon: 'blockquote',
        tooltip: 'Blockquote',
        onclick: function(e){
            var sel = editor.selection;
            if (sel.getNode().nodeName.toLowerCase() != 'blockquote') {
                var ct = sel.getContent();
                if (ct.length) {
                    editor.execCommand('mceInsertContent', false, '<blockquote>' + ct + '</blockquote>');
                    var currNode = editor.selection.getNode();
                    if (currNode) {
                        var nextNode = tinymce.activeEditor.dom.getNext(currNode, '*');
                        if (nextNode) {
                            var $br = $(nextNode.firstChild);
                            if ($br && $br[0].nodeType !== 3 && $br[0].tagName.toLowerCase() == 'br') {
                                $br.remove()
                            }
                        }
                    }
                } else {
                    editor.execCommand('FormatBlock', false, 'blockquote');
                }
            } else {
                editor.execCommand('FormatBlock', false, '');
            }
        },
        onPostRender: toggleState
    });

    editor.on('NodeChange', function(e) {
        editor.fire('blockquote', {state: e.element.nodeName.toLowerCase() == 'blockquote'});
    });

    function toggleState() {
        var self = this;
        editor.on('blockquote', function(e) {
            self.active(e.state);
        });
    }

});