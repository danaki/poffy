/*global tinymce:true */

tinymce.PluginManager.add('po_spoiler', function(editor, url) {

    function showDialog()
    {
        editor.windowManager.open({
            title: 'Скрытый текст',
            width: 580,
            height: 300,
            body: [
                { type: 'container', html: '<p style="margin-bottom: 16px;">Кат позволяет вам добавить в запись скрытый текст, что сделает ее более<br />удобной для ваших читателей. Добавьте текст в поле "кат" и укажите заголовок,<br />который будет отображаться в вашей записи</p>' },
                { type: 'textbox', name: 'cut_title', label: 'Заголовок' },
                { type: 'textbox', name: 'cut_text', label: 'Текст', multiline: true, minHeight: 150, value: editor.selection.getContent() }
            ],
            onsubmit: insertContent
        });
    }

    function insertContent(e)
    {
        var cut = '<teaser>' + e.data.cut_title + '</teaser><cut><p>' + e.data.cut_text.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '<br />') + '</p></cut>';
        editor.execCommand('mceInsertContent', false, cut);
    }

    editor.addButton('po_spoiler', {
        icon: false,
        image: url + '/img/icon.png',
        tooltip: 'Скрыть часть сообщения',
        onclick: showDialog,
        stateSelector: 'cut, teaser'
    });

    editor.on('keydown', function(e){
        if (e.keyCode == 13) {
            var currNode = editor.selection.getNode();
            if (currNode.nodeName.toLowerCase() == 'teaser') {
                var nextNode = editor.dom.getNext(currNode, 'cut');
                if (nextNode) {
                    editor.selection.setCursorLocation(nextNode.firstChild, 0);
                }
                return false;
            }
        }
    });

});