$(document).ready ->
  $("#new_contact_form").on("ajax:success", (e, data, status, xhr) ->
    $("#new_contact_form input").removeClass('error')
    if data.status == 'error'
      for error in data.errors
        $("#new_contact_form #contact_form_" + error).toggleClass('error')
    else
      $("#new_contact_form .contact_form_error").remove()
      $("#new_contact_form").append "<p class=\"contact_form_error\">" + data.message + "</p>"
      $("#new_contact_form [type=text]").val('')
  ).on "ajax:error", (e, xhr, status, error) ->
    $("#new_contact_form").append "<p>ERROR</p>"