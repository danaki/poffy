#= require jquery
#= require jquery.turbolinks
#= require jquery.ui.all
#= require jquery_ujs
#= require bootstrap
#= require tinymce-jquery
#= require jquery.Jcrop
#= require jquery.remotipart
#= require select2
#= require jquery.ui.nestedSortable
#= require sortable_tree/initializer
#= require turbolinks
#= require lazybox

#= require ./jquery.iframe-post-form
#= require_self

toggle_preview = (show) ->
  if (show)
    do $('#preview_container').show
    do $('#choose_preview_button').hide
    do $('#remove_preview_button').show
  else
    do $('#preview_container').hide
    do $('#choose_preview_button').show
    do $('#remove_preview_button').hide

jcrop_save_constrains = (coords) ->
  $('#preview_crop_x').val coords.x;
  $('#preview_crop_y').val coords.y;
  $('#preview_crop_w').val coords.w;
  $('#preview_crop_h').val coords.h;


attach_remote_replace = ->
  $('[data-remote][data-replace]')
    .on 'ajax:success', (event, data) ->
      $this = $(this);
      $($this.data('replace')).html(data);
      $this.trigger('ajax:replaced');

$(document).on 'ready page:load', ->
  do attach_remote_replace
  $('a[rel*=lazybox]').lazybox({overlay: true, esc: true, close: true, modal: true})

$(document).on 'page:fetch', ->
  $.lazybox("<i class='fa fa-spinner fa-spin'>", { klass: 'spinner', close: false, esc: false })

$(document).on 'page:change', ->
  $.lazybox.close()

$(document).on 'change', '#upload_filename', ->
  do $('#preview_upload_form').submit

$(document).on 'click', '#choose_preview_button', ->
  $('#preview_upload_form #upload_filename').trigger 'click';

$(document).on 'click', '#remove_preview_button', ->
  $('[name$=\\[resource_id\\]]').val null
  toggle_preview(false)

$(document).on 'click', '.remove_settings_image', (e) ->
  id = $(e.target).attr('rel')
  copy = $('#product_form #removed_images').clone()
  copy.val id
  $(copy).insertAfter('#product_form #removed_images')
  $('#product_form #' + id + '-container').remove()
  $(e.target).remove()

root = exports ? this
root.toggle_preview = toggle_preview
root.jcrop_save_constrains = jcrop_save_constrains

#$(document).ready(ready)
#$(document).on('page:load', ready)
