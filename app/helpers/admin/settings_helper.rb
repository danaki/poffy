module Admin::SettingsHelper
  require 'find'

  def show_rss_description
    Article.first.get_rss_description.html_safe rescue ""
  end
end
