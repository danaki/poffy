module Admin::DashboardHelper
  def dashboard_sidebar_link
    return unless current_user.can_access_to_customizesidebar?
    _("You can also %s to customize your Poffy blog.",
      link_to(_('download some plugins'), 'http://plugins.publify.co'))
  end

  def dashboard_action_links
    links = []

    links << link_to(_('write a post'), {:controller => 'content', :action => 'new'}, :class => 'alert-link') if current_user.can_access_to_articles?
    links << link_to(_('write a page'), :controller => 'pages', :action => 'new') if current_user.can_access_to_pages?
    links << link_to(_("update your profile or change your password"), :controller => 'profiles', :action => 'index') if current_user.can_access_to_profile?

    links.join(', ')
  end
end
