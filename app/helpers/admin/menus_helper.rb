module Admin::MenusHelper
  def show_menu_actions item
    content_tag(:div, {:class => 'action'}) do 
      [ small_to_edit(item),
        small_to_delete(item),
        ].join(" | ").html_safe
    end
  end
end
