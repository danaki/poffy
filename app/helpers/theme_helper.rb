module ThemeHelper
  # adds per theme helpers if file exists. Ugly but at least it works.
  # Use : just add your methods in yourtheme/helpers/theme_helper.rb
  unless Blog.default.nil?
    current_theme_path = ThemesForRails.config.interpolate(ThemesForRails.config.assets_dir, name)
    require "#{current_theme_path}/helpers/theme_helper.rb" if File.exists? "#{current_theme_path}/helpers/theme_helper.rb"
  end
end
