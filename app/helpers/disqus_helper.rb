module DisqusHelper

  def disqus_configured?
    this_blog.disqus_shortname.present?
  end

  def disqus_app_configured?
    this_blog.disqus_api_key.present?
  end

  def disqus_counts
    unless this_blog.disqus_shortname.empty?
      <<-HTML
        <script type="text/javascript">
          var disqus_shortname = '#{this_blog.disqus_shortname}';

          /* * * DON'T EDIT BELOW THIS LINE * * */
          (function () {
            var s = document.createElement('script'); s.async = true;
            s.type = 'text/javascript';
            s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
          }());
        </script>
      HTML
    end
  end

  def disqus_thread(article)
    unless this_blog.disqus_shortname.empty?
      <<-HTML
        <div id="disqus_thread"></div>
        <script type="text/javascript">
          var disqus_shortname = '#{this_blog.disqus_shortname}';
          var disqus_identifier = '#{article.id}';
          var disqus_url = '#{article.permalink_url only_path: false}';

          /* * * DON'T EDIT BELOW THIS LINE * * */
          (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
          })();
        </script>
      HTML
    end
  end

  # def disqus_recent_posts(count)
  #   if disqus_app_configured? then
  #     ids = []
  #     shortname = this_blog.disqus_shortname
  #     key = this_blog.disqus_api_key
  #     begin
  #       contents = Rails.cache.fetch("disqus_recent_posts", expires_in: 1.minute) do
  #         url = "http://disqus.com/api/3.0/posts/list.json?api_key=#{key}&forum=#{shortname}&limit=25&related=thread"
  #         open(url).read
  #       end
  #
  #       json_object = JSON.parse(contents)
  #       if json_object['code'] == 0 then
  #         json_object['response'].each do |item|
  #           ids << item['thread']['identifiers'][0]
  #         end
  #       end
  #
  #       ids.uniq!
  #     rescue
  #     end
  #
  #     if ids.count > 0 then
  #       Article.where(id: ids).take(count).to_a.sort_by { |a| ids.index(a.id.to_s) }
  #     else
  #       []
  #     end
  #   else
  #     nil
  #   end
  # end

end