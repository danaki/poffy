require 'open-uri'
require 'json'
require 'active_support/dependencies'

module ArticlesHelper

  # usage:
  #   link_to_next_page(@items)
  #   link_to_next_page(@items, :remote => true)  # Ajax
  def link_to_next_page(scope, name, options = {}, html_options = {}, &block)
    param_name = options.delete(:param_name) || Kaminari.config.param_name
    link_to_unless scope.last_page?, name, options.merge(param_name => (scope.current_page + 1)), html_options.merge(:rel => 'next') do
      block.call if block
    end
  end

  def sidebar_recent_disussions(count)
    send(Rails.configuration.poffy.sidebar_recent_disussions, count)
  end

  def sidebar_recent_posts(count)
    send(Rails.configuration.poffy.sidebar_recent_posts, count)
  end

  # def recent_articles(count)
  #   Article.recent.page(1).per(count)
  # end

  # def similar_posts(article)
  #   Article
  #     .joins(:tags)
  #     .where('categorizations.id' => article.tags)
  #     .where.not(id: article)
  #     .order('published_at DESC')
  #     .group('contents.id')
  #     .take(4)
  # end

  # def best_posts
  #   articles = Article.bestof_week.by_popularity.take(3)
  #   articles = Article.bestof_month.by_popularity.take(3) if articles.count == 0
  #   articles
  # end

  def cover_url(article, size, with_placeholder=true)
    if article.resource
      '/' + article.resource.path(size)
    elsif with_placeholder
      current_theme_image_path('preview/post-' + size.to_s + '.gif')
    else
      nil
    end
  end

  def cover_img(article, size, options={})
    blog = Blog.default
    constraints = Resource.thumbnails_for('article')[size]

    options.reverse_merge! with_placeholder: true, alt: article.title, title: article.title, size: constraints

    url = cover_url(article, size, options[:with_placeholder])
    options.delete :with_placeholder

    if url
      image_tag url, options
    else
      nil
    end
  end
end
