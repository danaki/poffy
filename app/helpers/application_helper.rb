# coding: utf-8
# Methods added to this helper will be available to all templates in the application.
require 'digest/sha1'

module ApplicationHelper
  # Need to rewrite this one, quick hack to test my changes.
  def page_title
    @page_title
  end

  def apply_to_title(template_name, item, params={})
    template = this_blog.send(template_name)
    template.to_title(item, this_blog, params)
  end

  #def product_path(params)
  #  super(params).gsub(/^\/products\/(.*)/, '\1')
  #end
  #
  #def tag_path(params)
  #  super(params).gsub(/^\/tag\/(.*)/, '\1')
  #end


  # Basic english pluralizer.
  # Axe?
  def pluralize(size, zero, one , many )
    case size
    when 0 then zero
    when 1 then one
    else        sprintf(many, size)
    end
  end

  # Produce a link to the permalink_url of 'item'.
  def link_to_permalink(item, title, anchor=nil, style=nil, nofollow=nil, only_path=false)
    options = {}
    options[:class] = style if style
    options[:rel] = "nofollow" if nofollow

    link_to title, item.permalink_url, options
  end

  # The '5 comments' link from the bottom of articles
  # def comments_link(article)
  #   comment_count = article.published_comments.size
  #   # FIXME Why using own pluralize metchod when the Localize._ provides the same funciotnality, but better? (by simply calling _('%d comments', comment_count) and using the en translation: l.store "%d comments", ["No nomments", "1 comment", "%d comments"])
  #   link_to_permalink(article,pluralize(comment_count, _('no comments'), _('1 comment'), _('%d comments', comment_count)),'comments', nil, nil, true)
  # end

  # def avatar_tag(options = {})
  #   avatar_class = this_blog.plugin_avatar.constantize
  #   return '' unless avatar_class.respond_to?(:get_avatar)
  #   avatar_class.get_avatar(options)
  # end
  #
  # def trackbacks_link(article)
  #   trackbacks_count = article.published_trackbacks.size
  #   link_to_permalink(article,pluralize(trackbacks_count, _('no trackbacks'), _('1 trackback'), _('%d trackbacks',trackbacks_count)),'trackbacks')
  # end

  #def meta_tag(name, value)
  #  tag :meta, :name => name, :articles => value unless value.blank?
  #end

  def feed_title
    if @feed_title.present?
      @feed_title
    elsif @page_title.present?
      @page_title
    else
      this_blog.blog_name
    end
  end

  def html(content, what = :all, deprecated = false)
    content.html(what)
  end

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end
  #
  # def display_user_avatar(user, size='avatar', klass='alignleft')
  #   if user.resource.present?
  #     avatar = case size
  #     when 'thumb'
  #       user.resource.upload.thumb.url
  #     when 'medium'
  #       user.resource.upload.medium.url
  #     when 'large'
  #       user.resource.upload.large.url
  #     else
  #       user.resource.upload.avatar.url
  #     end
  #   end
  #
  #   return unless avatar
  #   image_tag(File.join(this_blog.base_url, avatar), :alt => user.username, :class => klass)
  # end
  #
  # def author_picture(status)
  #   return if status.user.twitter_profile_image.nil? or status.user.twitter_profile_image.empty?
  #   return if status.twitter_id.nil? or status.twitter_id.empty?
  #
  #   image_tag(status.user.twitter_profile_image , class: "alignleft", alt: status.user.username)
  # end
  #
  #def view_on_twitter(status)
  #  return if status.twitter_id.nil? or status.twitter_id.empty?
  #  return " | " + link_to(_("View on Twitter"), File.join('https://twitter.com', status.user.twitter_account, 'status', status.twitter_id), {class: 'u-syndication', rel: 'syndication'})
  #end

  def google_tag_manager
    unless this_blog.google_analytics.empty?
      <<-HTML
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=#{this_blog.google_analytics}"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','#{this_blog.google_analytics}');</script>
        <!-- End Google Tag Manager -->
      HTML
    end
  end

  # def sidebar_article(article, from)
  #   link = article[:permalink_url]
  #   title = article[:title]
  #   id = article[:id]
  #
  #   <<-HTML
  #     <article class="post">
  #       <a href="#{ link }">#{ title }</a>
  #       <a class="po-icons comments" href="#{ link }#{ if disqus_configured? then '#disqus_thread" data-disqus-identifier="' + id.to_s end }"> </a>
  #     </article>
  #   HTML
  # end

  # def yandex_metrika
  #   unless this_blog.yandex_metrika.empty?
  #     <<-HTML
  #       <!-- Yandex.Metrika counter -->
  #       <script type="text/javascript">
  #       (function (d, w, c) {
  #           (w[c] = w[c] || []).push(function() {
  #               try {
  #                   w.yaCounter25201454 = new Ya.Metrika({id:#{this_blog.yandex_metrika},
  #                           webvisor:true,
  #                           clickmap:true,
  #                           trackLinks:true,
  #                           accurateTrackBounce:true});
  #               } catch(e) { }
  #           });
  #
  #           var n = d.getElementsByTagName("script")[0],
  #               s = d.createElement("script"),
  #               f = function () { n.parentNode.insertBefore(s, n); };
  #           s.type = "text/javascript";
  #           s.async = true;
  #           s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
  #
  #           if (w.opera == "[object Opera]") {
  #               d.addEventListener("DOMContentLoaded", f, false);
  #           } else { f(); }
  #       })(document, window, "yandex_metrika_callbacks");
  #       </script>
  #       <noscript><div><img src="//mc.yandex.ru/watch/#{this_blog.yandex_metrika}" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  #       <!-- /Yandex.Metrika counter -->
  #     HTML
  #   end
  # end

  #
  # def addthis
  #   unless this_blog.addthis_id.empty?
  #     <<-HTML
  #       <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=#{this_blog.addthis_id}"></script>
  #       <script>
  #
  #         $(function () {
  #           addthis.layers({
  #             'theme': 'transparent',
  #             'share': {
  #               'position': 'left',
  #               <% unless this_blog.addthis_services.empty? %>
  #                 'services': '#{this_blog.addthis_services}'
  #               <% end %>
  #             }
  #           });
  #         });
  #
  #       </script>
  #     HTML
  #   end
  # end

  def use_canonical
    "<link rel='canonical' href='#{this_blog.base_url + request.fullpath}' />".html_safe
  end

  def page_header
    render 'shared/page_header'
  end

  def page_header_includes
    #content_array.collect { |c| c.whiteboard }.collect do |w|
    #  w.select {|k,v| k =~ /^page_header_/}.collect do |_,v|
    #    v = v.chomp
    #    # trim the same number of spaces from the beginning of each line
    #    # this way plugins can indent nicely without making ugly source output
    #    spaces = /\A[ \t]*/.match(v)[0].gsub(/\t/, "  ")
    #    v.gsub!(/^#{spaces}/, '  ') # add 2 spaces to line up with the assumed position of the surrounding tags
    #  end
    #end.flatten.uniq.join("\n")
  end

  def feed_atom
    feed_for('atom')
  end

  def feed_rss
    feed_for('rss')
  end

  #def render_the_flash
  #  return unless flash[:notice] or flash[:error] or flash[:warning]
  #  the_class = flash[:error] ? 'error' : 'success'
  #
  #  html = "<div class='alert alert-#{the_class}'>"
  #  html << "<a class='close' href='#'>×</a>"
  #  html << render_flash rescue nil
  #  html << "</div>"
  #  html.html_safe
  #end

  def content_array
    if @articles
      @articles
    elsif @article
      [@article]
    elsif @page
      [@page]
    else
      []
    end
  end

  def new_js_distance_of_time_in_words_to_now(date)
    # Ruby Date class doesn't have #utc method, but _publify_dev.html.erb
    # passes Ruby Date.
    date = date.to_time
    time = _(date.utc.strftime(_("%%a, %%d %%b %%Y %%H:%%M:%%S GMT", date.utc)))
    timestamp = date.utc.to_i
    content_tag(:span, time, {:class => "publify_date date gmttimestamp-#{timestamp}", :title => time})
  end

  def display_date(date)
    date.strftime(this_blog.date_format)
  end

  def display_time(time)
    time.strftime(this_blog.time_format)
  end

  def display_date_and_time(timestamp)
    return new_js_distance_of_time_in_words_to_now(timestamp) if this_blog.date_format == 'distance_of_time_in_words'
    "#{display_date(timestamp)} at #{display_time(timestamp)}"
  end

  #def show_meta_keyword
  #  return unless this_blog.use_meta_keyword
  #  meta_tag 'keywords', @keywords unless @keywords.blank?
  #end

  def this_blog
    @blog ||= Blog.default
  end

  def stop_index_robots?
    stop = (params[:year].present? || params[:page].present?)
    # stop = @blog.unindex_tags if controller_name == "tags"
    # stop = @blog.unindex_categories if controller_name == "categories"
    stop
  end
  #
  # def posts_sidebar(articles, title, from)
  #   render partial: 'articles/posts_sidebar', locals: { articles: articles, title: title, from: from} if articles.count > 0
  # end

  #def main_categories
  #  Category.main
  #end
  #
  #def subcategories_for(main)
  #  Category.subs_for(main)
  #end

  private

  def feed_for(type)
    if not this_blog.feedburner_url.empty?
      "http://feeds2.feedburner.com/#{this_blog.feedburner_url}"
    else
      url_for(controller: '/articles', action: :index, devise: false, format: type, only_path: false)
    end
  end

  def bootstrap_class_for flash_type
    case flash_type.to_sym
      when :success
        "alert-success" # Green
      when :error
        "alert-danger" # Red
      when :alert
        "alert-warning" # Yellow
      when :notice
        "alert-info" # Blue
      else
        flash_type.to_s
    end
  end

  def html_editor(*additional_plugins)
    config = tinymce_configuration.options_for_tinymce
    config['content_css'] = stylesheet_path('tinymce/tinymce');

    if additional_plugins.present? then
      config['plugins'] += ',' + additional_plugins.join(',')
      config['toolbar2'] += " | "  + additional_plugins.join(' | ')
    end
    # raise tinymce(config).inspect
    tinymce(config)
  end

  #def render_flash
  #  output = []
  #  for key,value in flash
  #    output << "<span class=\"#{key.to_s.downcase}\">#{h(_(value))}</span>"
  #  end if flash
  #  output.join("<br />\n")
  #end

  def widget(position, *params)
    controller = @_controller.controller_name.to_sym
    action     = @_controller.action_name.to_sym
    config = Rails.configuration.poffy.widgets
    contoller_config = config[controller].presence
    page_config = config[:default].merge((contoller_config && contoller_config[action].presence) || {})
    widgets = page_config[position.to_sym].presence || []
    [*widgets].map(&proc { |w| cell(w.to_sym, *params).call(:show) }).join.html_safe
  end

end

module BootstrapForm
  class FormBuilder
    def tinymce_area(method, options = {})
      options.merge!(
        class: "tinymce"
      )

      text_area(method, options)

    end
  end
end