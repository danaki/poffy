module IpbHelper
  def ipb_rss(feed_id, count)
    begin
      url = "http://forum.apoker.kz/index.php?app=core&module=global&section=rss&type=forums&id=#{feed_id}"
      contents = Rails.cache.fetch("ipb_rss_#{feed_id}", expires_in: 1.minute) do
        open(url).read
      end

      rss = Hash.from_xml(contents)
      rss['rss']['channel']['item'].slice(0, count).map { |a|
        o = ActiveSupport::OrderedOptions.new
        o.merge!({
          permalink_url: a['link'].to_s + "?view=getlastpost",
          title: a['title'],
          id: nil
        })
      }
    rescue
      nil
    end
  end

  def ipb_recent_posts(count)
    ipb_rss(1, count)
  end

  def ipb_recently_commented(count)
    ipb_rss(2, count)
  end
end