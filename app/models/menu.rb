class Menu < ActiveRecord::Base
  include TheSortableTree::Scopes
  acts_as_nested_set

  validates_presence_of :title
end