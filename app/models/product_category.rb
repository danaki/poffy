class ProductCategory < Category
  include Validators
  validates :slug, global_slug: true, :if => Proc.new { |f| f.slug.present? }

  has_and_belongs_to_many :products, -> { order 'published_at DESC' }, join_table: 'categorizations', association_foreign_key: 'article_id', foreign_key: 'category_id'

  def permalink_url
    id = self_and_ancestors.map { |c| c.slug }.join('/')

    blog = Blog.default # remove me...
    blog.url_for(controller: 'products', action: 'show', id: id, only_path: true)
  end

  protected

  def should_generate_new_friendly_id?
    false
  end

end