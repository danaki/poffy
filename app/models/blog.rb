# The Blog class represents the one and only blog.  It stores most
# configuration settings and is linked to most of the assorted content
# classes via has_many.
#
# Once upon a time, there were plans to make publify handle multiple blogs,
# but it never happened and publify is now firmly single-blog.
#
class Blog < ActiveRecord::Base
  include ConfigManager
  include Rails.application.routes.url_helpers

  attr_accessor :custom_permalink

  store_accessor :settings

  validate(:on => :create) { |blog|
    unless Blog.count.zero?
      blog.errors.add(:base, "There can only be one...")
    end
  }

  validates :blog_name, :presence => true

  # Description
  setting :blog_name,                     :string,  default: 'Poffy!'
  setting :blog_subtitle,                 :string

  # Mostly Behaviour
  setting :limit_article_display,         :integer, default: 10
  setting :limit_rss_display,             :integer, default: 10
  setting :default_allow_comments,        :boolean, default: true
  setting :link_to_author,                :boolean, default: false
  setting :plugin_avatar,                 :string
  setting :email_from,                    :string,  default: 'poffy@example.com'
  setting :date_format,                   :string,  default: '%d/%m/%Y'
  setting :time_format,                   :string,  default: '%Hh%M'

  # SEO
  setting :description,                   :string
  #setting :meta_keywords,                 :string
  setting :google_analytics,              :string
  setting :feedburner_url,                :string
  setting :rss_description,               :boolean
  setting :rss_description_text,          :string, default: "<hr /><p><small>Original article writen by %author% and published on <a href='%blog_url%'>%blog_name%</a> | <a href='%permalink_url%'>direct link to this article</a> | If you are reading this article elsewhere than <a href='%blog_url%'>%blog_name%</a>, it has been illegally reproduced and without proper authorization.</small></p>"
  #setting :permalink_format,           :string, '/%year%/%month%/%day%/%title%'
  setting :robots,                        :string
  setting :admin_display_elements,        :integer, default: 25
  setting :dofollowify,                   :boolean, default: false
  setting :use_canonical_url,             :boolean, default: false
  setting :use_meta_keyword,              :boolean, default: true

  # title templates
  setting :home_title_template,           :string, default: "%blog_name% | %blog_subtitle%" # spec OK
  setting :home_desc_template,            :string, default: "%description%" # OK
  setting :article_title_template,        :string, default: "%title% | %blog_name%"
  # setting :article_desc_template,         :string, default: "%description%"
  setting :page_title_template,           :string, default: "%title% | %blog_name%"
  # setting :page_desc_template,            :string, default: "%description%"
  setting :paginated_title_template,      :string, default: "%blog_name% | %blog_subtitle% %page%"
  setting :paginated_desc_template,       :string, default: "%blog_name% | %blog_subtitle% | %page%"
  setting :products_list_title_template,  :string, default: "%title% | %blog_name%"
  setting :products_list_desc_template,   :string, default: "%description%"
  setting :product_title_template,        :string, default: "%meta_title% | %blog_name%"
  # setting :product_desc_template,         :string, default: "%description%"
  setting :theme_title_template,          :string, default: "Theme: %title% | %blog_name% %page%"
  setting :theme_desc_template,           :string, default: "%description%"
  setting :tag_title_template,            :string, default: "Tag: %title% | %blog_name% %page%"
  setting :tag_desc_template,             :string, default: "%description%"
  setting :author_title_template,         :string, default: "%author% | %blog_name%"
  setting :author_desc_template,          :string, default: "%author% | %blog_name% | %blog_subtitle%"
  setting :archives_title_template,       :string, default: "Archives for %blog_name% %date% %page%"
  setting :archives_desc_template,        :string, default: "Archives for %blog_name% %date% %page% %blog_subtitle%"
  setting :search_title_template,         :string, default: "Results for %search% | %blog_name% %page%"
  setting :search_desc_template,          :string, default: "Results for %search% | %blog_name% | %blog_subtitle% %page%"

  setting :custom_url_shortener,          :string
  setting :custom_tracking_field,         :string
  # setting :meta_author_template,       :string, "%blog_name% | %username%"

  setting :google_verification,           :string
  setting :disqus_shortname,              :string
  setting :disqus_api_key,                :string
  setting :twitter_consumer_key,          :string
  setting :twitter_consumer_secret,       :string

  setting :carousel_banners_list,         :text

  #validate :permalink_has_identifier

  def initialize(*args)
    super
    # Yes, this is weird - PDC
    begin
      self.settings ||= {}
    rescue Exception => e
      self.settings = {}
    end
  end

  # The default Blog. This is the lowest-numbered blog, almost always
  # id==1. This should be the only blog as well.
  def self.default
    #find(:first, :order => 'id')
    first
  rescue
    logger.warn 'You have no blog installed.'
    nil
  end

  # Check that all required blog settings have a value.
  def configured?
    settings.has_key?('blog_name')
  end

  def current_theme
    @current_theme ||= Rails.configuration.poffy.theme
  end

  def product_model
    Rails.configuration.poffy.products.model
  end


  # Generate a URL based on the +base_url+.  This allows us to generate URLs
  # without needing a controller handy, so we can produce URLs from within models
  # where appropriate.
  #
  # It also caches the result in the RouteCache, so repeated URL generation
  # requests should be fast, as they bypass all of Rails' route logic.
  def url_for_with_base_url(options = {}, extra_params = {})
    case options
    when String
      if extra_params[:only_path]
        url_generated = root_path
      else
        url_generated = base_url
      end

      url_generated += "/#{options}" # They asked for 'url_for "/some/path"', so return it unedited.
      url_generated += "##{extra_params[:anchor]}" if extra_params[:anchor]
      url_generated
    when Hash
      unless RouteCache[options]
        options.reverse_merge!(:only_path => false, :controller => '',
                               :action => 'permalink',
                               :host => host_with_port,
                               :script_name => root_path)

        RouteCache[options] = url_for_without_base_url(options)
      end

      return RouteCache[options]
    else
      raise "Invalid URL in url_for: #{options.inspect}"
    end
  end

  alias_method_chain :url_for, :base_url

  # The URL for a static file.
  def file_url(filename)
    if CarrierWave.configure { |config| config.storage == CarrierWave::Storage::Fog }
      filename
    else
      url_for "files/#{filename}", :only_path => false
    end
  end

  def articles_matching(query, args={})
    Article.search(query, args)
  end

  def per_page(format)
    return limit_article_display if format.nil? || format == 'html'
    limit_rss_display
  end

  def rss_limit_params
    limit = limit_rss_display.to_i
    return limit.zero? \
      ? {} \
      : {:limit => limit}
  end

  #def permalink_has_identifier
  #  unless permalink_format =~ /(%title%)/
  #    errors.add(:permalink_format, _("You need a permalink format with an identifier : %%title%%"))
  #  end
  #
  #  # A permalink cannot end in .atom or .rss. it's reserved for the feeds
  #  if permalink_format =~ /\.(atom|rss)$/
  #    errors.add(:permalink_format, _("Can't end in .rss or .atom. These are reserved to be used for feed URLs"))
  #  end
  #end

  def root_path
    split_base_url[:root_path]
  end

  def transliteration_options
    :russian
  end

  #def text_filter_object
  #  text_filter.to_text_filter
  #end

  def has_twitter_configured?
    return false if self.twitter_consumer_key.nil? or self.twitter_consumer_secret.nil?
    return false if self.twitter_consumer_key.empty? or self.twitter_consumer_secret.empty?
    true
  end

  private

  def protocol
    split_base_url[:protocol]
  end

  def host_with_port
    split_base_url[:host_with_port]
  end

  def split_base_url
    unless @split_base_url
      unless base_url =~ /(https?):\/\/([^\/]*)(.*)/
        raise "Invalid base_url: #{self.base_url}"
      end
      @split_base_url = { :protocol => $1, :host_with_port => $2,
                          :root_path => $3.gsub(%r{/$},'') }
    end
    @split_base_url
  end

end

