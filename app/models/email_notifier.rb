class EmailNotifier < ActiveRecord::Observer
  observe Article

  def after_save(content)
    content.send_notifications
    true
  end
end
