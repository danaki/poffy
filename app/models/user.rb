require 'digest/sha1'

# Poffy user.
class User < ActiveRecord::Base
  rolify

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook, :twitter]

  # Virtual attribute for authenticating by either username or email
  # This is in addition to a real persisted field like 'username'
  #attr_accessor :username
  attr_accessor :login

  include ConfigManager

#  belongs_to :text_filter
  belongs_to :resource

  has_many :notifications, :foreign_key => 'notify_user_id'
  #has_many :notify_contents, :through => :notifications,
  #  :source => 'notify_content',
  #  :uniq => true

  has_many :notify_contents, -> { uniq }, :through => :notifications,
           :source => 'notify_content'

  has_many :articles

  store_accessor :settings

  attr_accessor :filename

  # Settings
  setting :name,                       :string
  setting :description,                :string
  setting :url,                        :string

  # echo "publify" | sha1sum -
  class_attribute :salt

  def self.salt
    '20ac4d290c2293702c64b3b287ae5ea79b26a5c1'
  end

  attr_accessor :last_venue

  def initialize(*args)
    super
    self.settings ||= {}
  end

  def first_and_last_name
    return '' unless firstname.present? && lastname.present?
    "#{firstname} #{lastname}"
  end

  def display_names
    [:username, :username, :firstname, :lastname, :first_and_last_name].map{|f| send(f)}.delete_if{|e| e.empty?}
  end

  #def self.authenticate(login, pass)
  #  where("login = ? AND password = ? AND state = ?", login, password_hash(pass), 'active').first
  #end

  def update_connection_time
    self.last_venue = last_connection
    self.last_connection = Time.now
    self.save
  end

  ## These create and unset the fields required for remembering users between browser closes
  #def remember_me
  #  remember_me_for 2.weeks
  #end
  #
  #def remember_me_for(time)
  #  remember_me_until time.from_now.utc
  #end
  #
  #def remember_me_until(time)
  #  self.remember_token_expires_at = time
  #  self.remember_token            = Digest::SHA1.hexdigest("#{email}--#{remember_token_expires_at}")
  #  save(:validate => false)
  #end
  #
  #def forget_me
  #  self.remember_token_expires_at = nil
  #  self.remember_token            = nil
  #  save(:validate => false)
  #end

  def permalink_url(anchor=nil, only_path=false)
    blog = Blog.default # remove me...

    blog.url_for(
      :controller => 'authors',
      :action => 'show',
      :id => username,
      :only_path => only_path
    )
  end

  #def default_text_filter
  #  text_filter
  #end

  #def self.authenticate?(username, pass)
  #  user = self.authenticate(username, pass)
  #  return false if user.nil?
  #  return true if user.login == username
  #
  #  false
  #end

  def self.find_by_permalink(permalink)
    self.find_by_username(permalink).tap do |user|
      raise ActiveRecord::RecordNotFound unless user
    end
  end

  def self.to_prefix
    'author'
  end

  def article_counter
    articles.size
  end

  def display_name
    if !username.blank?
      username
    elsif !name.blank?
      name
    else
      username
    end
  end

  def permalink
    username
  end

  def admin?
    roles.collect {|role| role.name }.include?('admin')
  end

  def update_twitter_profile_image(img)
    return if self.twitter_profile_image == img
    self.twitter_profile_image = img
    self.save
  end

  def generate_password!
    chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a
    newpass = ""
    1.upto(7) { |i| newpass << chars[rand(chars.size-1)] }
    self.password = newpass
  end

  def has_twitter_configured?
    self.twitter_oauth_token.present? && self.twitter_oauth_token_secret.present?
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end

  protected


  validates_uniqueness_of :username, :on => :create
  #validates_uniqueness_of :email, :on => :create

  validates_presence_of :username
  #validates_presence_of :email

end
