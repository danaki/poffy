class Tag < Category
  include Validators
  validates :slug, global_slug: true
  validates_format_of :slug, :with => /\A[a-z0-9-]+\z/

  has_and_belongs_to_many :articles, -> { order 'published_at DESC' }, join_table: 'categorizations', foreign_key: 'category_id'

  def published_articles
    articles.already_published
  end

  protected

    def should_generate_new_friendly_id?
      slug.blank?
    end
end
