# coding: utf-8
class PostType < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged]
  validates_format_of :slug, :with => /\A[a-z0-9-]+\z/
  #validates_uniqueness_of :slug

  include Validators
  validates :slug, global_slug: true

  belongs_to :content

  validates_uniqueness_of :title
  validates_presence_of :title
  validates_length_of :title, :maximum => 255
  validate :title_is_not_read

  default_scope { order('slug') }

  def title_is_not_read
    errors.add(:title, "This article type already exists") if title == 'read'
  end

  protected

    def should_generate_new_friendly_id?
      slug.blank?
    end

end
