module ConfigManager
  # def self.included(base)
  #   (@@bases ||= []) << base
  # end
  #
  # def self.calling_classes
  #   @@bases
  # end

  def self.append_features(base)
    super
    base.extend(ClassMethods)
  end

  module ClassMethods

    def fields_for(klass)
      fields.select { |key,_| klass.ancestors.include? key }
    end

    def fields_for_merged(klass)
      fields_for(klass).map { |_,v| v }.reduce(Hash.new, :merge)
    end

    def fields_for_merged_self
      fields_for_merged(self)
    end

    def setting(name, type=:object, params={})
      default = case type
                  when :boolean
                    false
                  when :integer
                    0
                  when :string
                    ''
                  else
                    nil
                end

      params = {
        default: default,
        title: name.to_s.humanize,
        help: nil
      }.merge(params)

      item = Item.new
      item.name, item.ruby_type, item.params = name.to_s, type, params
      # item.base = self

      self.fields[self][name.to_s] = item
      # puts self.fields.inspect

      add_setting_accessor(item)
    end

    def default_for(key)
      fields[self][key.to_s].params[:default]
    end

    def fields
      self.send(:class_attribute, :fields)
      self.send(:cattr_accessor, :fields)

      self.fields ||= Hash.new { |hash, key| hash[key] = Hash.new { Item.new } }
    end

    protected

    private

    def add_setting_accessor(item)
      add_setting_reader(item)
      add_setting_writer(item)
    end

    def add_setting_reader(item)
      self.send(:define_method, item.name) do
        raw_value = settings[item.name]
        raw_value.nil? ? item.params[:default] : raw_value
      end

      if item.ruby_type == :boolean
        self.send(:define_method, item.name + "?") do
          raw_value = settings[item.name]
          raw_value.nil? ? item.params[:default] : raw_value
        end
      end
    end

    def add_setting_writer(item)
      self.send(:define_method, "#{item.name}=") do |newvalue|
        self.settings ||= {}
        self.settings_will_change!
        retval = settings[item.name] = canonicalize(self, item.name, newvalue)
        retval
      end
    end

  end

  def canonicalize(klass, key, value)
    self.class.fields[klass][key.to_s].canonicalize(value)
  end

  class Item
    attr_accessor :name, :ruby_type, :params
    # attr_accessor :base

    def canonicalize(value)
      case ruby_type
      when :boolean
        case value
        when "0", 0, '', false, "false", "f", nil
          false
        else
          true
        end
      when :integer
        value.to_i
      when :string
        value.to_s
      when :enum
        value
      when :yaml
        value.to_yaml
      else
        value
      end
    end
  end
end
