class Product < Content
  include PoffyGuid
  include ConfigManager

  belongs_to :user
  belongs_to :resource

  setting :linked_url, :string, title: "Linked URL", help: "2 An anchor of cover image. It's safe to leave empty"
  setting :shortname, :string
  setting :icon, :image
  # setting :url, :string
  setting :prio, :integer
  setting :rating, :integer
  setting :header_top, :string
  setting :header_middle, :text
  setting :button_label, :string, help: "Product list page. If empty -- no button will be shown", title: "Product list button"
  setting :main_button_label, :string, help: "On product page is the Big Red Button over the heading image"
  setting :bottom_button_label, :string, help: "On product page is the Big Green Button on the bottom"

  before_validation { process_settings(self.class) }

  before_create :create_guid
  before_save :set_published_at, :set_temporary_slug

  after_save :shorten_url

  validates_presence_of :title, :shortname
  validates_length_of :title, :maximum => 255

  has_and_belongs_to_many :product_categories, join_table: 'categorizations', association_foreign_key: 'category_id', foreign_key: 'article_id'

  has_many :related_articles_association, :class_name => "RelatedContent", :foreign_key => "related_content_id"
  has_many :related_articles, :through => :related_articles_association, :source => :article

  def self.default_order
    "settings -> 'prio' ASC"
  end

  def self.init_url_helpers(klass)
    self.fields_for_merged(klass).each_value do |item|
      case item.ruby_type
        when :image
          define_method("#{item.name}_url") do
            value = self.send(item.name)
            "/files/product/#{value}" unless value.nil?
          end
      end
    end
  end

  def normalize_friendly_id(input)
    input.to_s.to_slug.normalize(transliterate: Rails.configuration.poffy.transliteration.to_sym).to_s
  end

  def permalink_url
    blog = Blog.default # remove me...
    blog.url_for(controller: 'products', action: 'show', id: if slug.to_s == "" then id else slug end, only_path: true)
  end

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end

end