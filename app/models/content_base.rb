module ContentBase
  def self.included base
    base.extend ClassMethods
  end

  def blog
    @blog ||= Blog.default
  end

  attr_accessor :just_changed_published_status
  alias_method :just_changed_published_status?, :just_changed_published_status

  # Set the text filter for this object.
  #def text_filter= filter
  #  filter_object = filter.to_text_filter
  #  if filter_object
  #    self.text_filter_id = filter_object.id
  #  else
  #    self.text_filter_id = filter.to_i
  #  end
  #end

  def really_send_notifications
    interested_users.each do |value|
      send_notification_to_user(value)
    end
    return true
  end

  def send_notification_to_user(user)
    notify_user_via_email(user)
  end

  # Return HTML for some part of this object.
  def html(field)
    generate_html(field)
  end

  # Generate HTML for a specific field using the text_filter in use for this
  # object.
  def generate_html(field, text = nil)
    text ||= self[field].to_s
    prehtml = html_preprocess(field, text).to_s
  end

  # Post-process the HTML.  This is a noop by default, but Comment overrides it
  # to enforce HTML sanity.
  def html_postprocess(field, html)
    html
  end

  def html_preprocess(field, html)
    html.gsub(/\[cards(\s|&nbsp;)+[^\]]*\]/) do |shortcode|
      rval = ''
      matches = shortcode.match(/^\[cards(\s|&nbsp;)+([^\]]*)\s*\]$/)
      if ! matches.nil? then
        matches[2].split(/(\s|&nbsp;)+/).each do |code|
          code.gsub(/^([^\s]+)$/) do |card|
            _, face, suit = card.match(/^(.{1,2})(.{1})$/).to_a
            color = if ['h', 'd'].include? suit then 'red' else 'black' end
            entities = {
              'h' => '&hearts;',
              'd' => '&diams;',
              'c' => '&clubs;',
              's' => '&spades;'
            }

            rval += "<span style=\"color: #{color}\">#{face}" + entities[suit].to_s + "</span>"
          end
        end
        rval
      else
        shortcode
      end
    end
  end

  def excerpt_text(length = 160)
    text = html(:body)
    text = (HTMLEntities.new.decode text).strip_html
    #text = ActionView::Base.full_sanitizer.sanitize(html)

    return text.truncate(length, separator: '');
  end

  def invalidates_cache?(on_destruction = false)
    @invalidates_cache ||= if on_destruction
      just_changed_published_status? || published?
    else
      (changed? && published?) || just_changed_published_status?
    end
  end

  def publish!
    self.published = true
    self.state = :published # dunno why this not triggered
    self.save!
  end

  # The default text filter.  Generally, this is the filter specified by blog.text_filter,
  # but comments may use a different default.
  #def default_text_filter
  #  blog.text_filter_object
  #end


  module ClassMethods
    def find_published(what = :all, options = {})
      where(:published => true).order(default_order).scoping do
        find what, options
      end
    end

    # def default_order
    #   'published_at DESC'
    # end
  end
end
