class Category < ActiveRecord::Base
  include ConfigManager

  store_accessor :settings

  setting :meta_title, :string
  setting :meta_description, :text

  include TheSortableTree::Scopes
  acts_as_nested_set

  extend FriendlyId
  friendly_id :title, use: [:slugged]

  validates_presence_of :title
  validates_length_of :title, :maximum => 255
  #validates_uniqueness_of :title, :on => :create

  after_initialize :init

  def init
    # Yes, this is weird - PDC
    begin
      self.settings ||= {}
    rescue
      self.settings = {}
    end
  end

  def title_with_path
    if parent.nil? then
      title
    else
      parent.title_with_path + ' > ' + title
    end
  end

  def self.to_prefix
    'category'
  end

  def published_articles
    articles.already_published
  end

  def normalize_friendly_id(input)
    input.to_s.to_slug.normalize(transliterate: Rails.configuration.poffy.transliteration.to_sym).to_s
  end

  def permalink_url
    blog = Blog.default # remove me...
    blog.url_for(controller: self.class.to_s.tableize, action: 'show', id: if slug.to_s == "" then id else slug end, only_path: true)
  end

end

