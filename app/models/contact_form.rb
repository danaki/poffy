class ContactForm < MailForm::Base
  attribute :name,  :validate => true
  attribute :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :skype

  def blog
    @blog ||= Blog.default
  end

  def headers
    {
      subject: "Contact Form",
      to:      blog.email_from,
      from:    %("#{name}" <#{email}>)
    }
  end
end