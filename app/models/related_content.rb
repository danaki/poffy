class RelatedContent < ActiveRecord::Base
  belongs_to :article
  belongs_to :related_content, class_name: Rails.configuration.poffy.products.model.name
end