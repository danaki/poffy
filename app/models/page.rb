class Page < Content
  belongs_to :user
  validates_presence_of :title, :body

  #include ConfigManager

  #store_accessor :settings
  #setting :password, :string, ''

  #before_save :set_permalink
  after_save :shorten_url

  #def set_permalink
  #  self.name = self.title.to_permalink if self.name.blank?
  #end

  # def initialize(*args)
  #   super
  #   # Yes, this is weird - PDC
  #   begin
  #     self.settings ||= {}
  #   rescue Exception => e
  #     self.settings = {}
  #   end
  # end

  def self.search_with(search_hash)
    super(search_hash).order('title ASC')
  end

  def permalink_url
    blog = Blog.default # remove me...
    blog.url_for(controller: 'articles', action: 'view_page', id: if slug.to_s == "" then id else slug end, only_path: true)
  end


end
