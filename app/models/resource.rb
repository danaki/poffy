class Resource < ActiveRecord::Base
  has_one :article, dependent: :nullify

  #mount_uploader :upload, ResourceUploader

  scope :without_images, lambda { where("mime NOT LIKE '%image%'") }
  scope :images, lambda { where("mime LIKE '%image%'") }
  scope :by_filename, lambda { order("upload") }
  scope :by_created_at, lambda { order("created_at DESC") }

  scope :without_images_by_filename, lambda { without_images.by_filename }
  scope :images_by_created_at, lambda { images.by_created_at }

  def crop(original, config, crop_x, crop_y, crop_w, crop_h)
    src_dir = "#{Rails.public_path}/#{self.class.original_dir}"
    dst_dir = "#{Rails.public_path}/#{dir}"

    FileUtils.mkdir_p(dst_dir) unless File.directory?(dst_dir)

    ext = File.extname(original).downcase

    blob = MiniMagick::Image.open("#{src_dir}/#{original}")

    image = MiniMagick::Image.read(blob)
    image.crop("#{crop_w}x#{crop_h}+#{crop_x}+#{crop_y}!")
    image.write("#{dst_dir}/crop#{ext}")

    config.each do |size, dimensions|
      thumb = image.clone
      thumb.combine_options do |c|
        c.unsharp("2x0.5+0.5+0")
        c.resize("#{dimensions}!")
        c.quality(100)
        # c.sharpen(1)
      end
      thumb.write("#{dst_dir}/#{size}#{ext}")
    end
  end

  def self.upload_dir
    "files"
  end

  def self.original_dir
    "#{upload_dir}/original"
  end

  def self.resource_dir
    "#{upload_dir}/resource"
  end

  def dir
    "#{self.class.resource_dir}/#{self.id.to_s}"
  end

  def path(size)
    "#{dir}/#{size}#{self.ext}"
  end

  def self.thumbnails_for(kind)
    config = {
      'article' => {
        full:    "690x460",
        big:     "328x218",
        big2:    "510x340",
        medium:  "240x160",
        medium2: "210x140",
        small:   "148x98"
      },
      'product' => {
        full: "1110x370"
      }
    }

    config[kind]
  end

  #
  #def resize_and_crop(blob, w0, h0, w1, h1)
  #  image = MiniMagick::Image.read(blob)
  #
  #  rw = w1.to_f / w0
  #  rh = h1.to_f / h0
  #
  #  unless rw > 1 or rh > 1
  #    w,h = w1, h1
  #    sx, sy = 0, 0
  #
  #    if rw > rh
  #      h = (h0 * rw).to_i
  #      sy = (h - h1) / 2
  #    else
  #      w = (w0 * rh).to_i
  #      sx = (w - w1) / 2
  #    end
  #
  #    image.thumbnail("#{w}x#{h}")
  #    image.crop("#{w1}x#{h1}+#{sx}+#{sy}")
  #  end
  #
  #  image.to_blob
  #end
end
