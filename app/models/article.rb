# coding: utf-8
require 'uri'
require 'net/http'

class Article < Content
  include PoffyGuid
  include ConfigManager
  include ActionView::Helpers::AssetTagHelper

  setting :linked_url, :string, title: "Linked URL", help: "An anchor of cover image. It's safe to leave empty"

  before_validation { process_settings(self.class) }

  validates_uniqueness_of :guid
  validates_presence_of :title
  validates_length_of :title, :maximum => 255
  #validates_presence_of :slug

  belongs_to :user
  belongs_to :resource

  has_and_belongs_to_many :themes, join_table: 'categorizations', association_foreign_key: 'category_id', :uniq => true
  has_and_belongs_to_many :tags, join_table: 'categorizations', association_foreign_key: 'category_id', :uniq => true

  #has_many :categorizations
  #has_many :themes, through: :categorizations
  accepts_nested_attributes_for :themes, :allow_destroy => true

  has_many :related_contents_association, :class_name => "RelatedContent"
  has_many :related_contents,
    through: :related_contents_association,
    source: :related_content

  has_many :triggers, as: :pending_item

  before_validation { process_settings(self.class) }
  before_create :create_guid
  before_save :set_published_at, :ensure_settings_type, :set_temporary_slug #, :set_permalink
  after_save :post_trigger, :shorten_url#, :keywords_to_tags

  #scope :category, lambda { |category_id| where('categorizations.category_id = ?', category_id).includes('categorizations') }
  scope :drafts, lambda { where(state: 'draft').order('created_at DESC') }
  scope :child_of, lambda { |article_id| where(parent_id: article_id) }
  scope :published_at, lambda {|time_params| published.where(published_at: PoffyTime.delta(*time_params)).order('published_at DESC')}
  scope :published_since, lambda {|time| published.where('published_at > ?', time) }
  scope :withdrawn, lambda { where(state: 'withdrawn').order('published_at DESC') }
  scope :pending, lambda { where('state = ? and published_at > ?', 'publication_pending', Time.now).order('published_at DESC') }
  scope :recent, -> () { where(["published = ?", true]).order('published_at DESC') }
  scope :bestof_week, ->() { published_since(DateTime.now - 7.days) }
  scope :bestof_month, ->() { published_since(DateTime.now - 30.days) }

  attr_accessor :draft, :keywords

  include Article::States

  has_state(:state,
    valid_states: [
      :new,
      :draft,
      :publication_pending,
      :just_published,
      :published,
      :just_withdrawn,
      :withdrawn
    ],
    initial_state: :new,
    :handles => [
      :withdraw,
      :post_trigger,
      :send_notifications,
      :published_at=,
      :just_published?
    ])


  #def set_permalink
  #  return if self.state == 'draft' || self.permalink.present?
  #  self.permalink = self.title.to_permalink
  #end

  def set_author(user)
    self.user = user
  end

  def has_child?
    Article.exists?(parent_id: self.id)
  end

  def post_type
    _post_type = read_attribute(:post_type)
    _post_type = 'read' if _post_type.blank?
    _post_type
  end

  def self.last_draft(article_id)
    article = Article.find(article_id)
    while article.has_child?
      article = Article.child_of(article.id).first
    end
    article
  end

  def self.search_with(params)
    params ||= {}
    scoped = super(params)
    if ["no_draft", "drafts", "published", "withdrawn", "pending"].include?(params[:state])
      scoped = scoped.send(params[:state])
    end

    if params[:category] && params[:category].to_i > 0
      scoped = scoped.category(params[:category])
    end

    scoped.order('created_at DESC')
  end

  def permalink_url(options = {})
    options[:only_path] = true if options[:only_path].nil?

    format_url = '/%slug%-post-%id%'
    format_url.gsub!('%id%', id.to_s)
    format_url.gsub!('%slug%', URI.encode(slug.to_s))

    if format_url[0,1] == '/'
      format_url = format_url[1..-1]
    end

    blog.url_for(format_url, options)
  end

  #def save_attachments!(files)
  #  files ||= {}
  #  files.values.each { |f| self.save_attachment!(f) }
  #end
  #
  #def save_attachment!(file)
  #  self.resources << Resource.create_and_upload(file)
  #rescue => e
  #  logger.info(e.message)
  #end

  # def feed_url(format)
  #   "#{permalink_url}.#{format.gsub(/\d/,'')}"
  # end

  def next
    Article.where('published_at > ?', published_at).order('published_at asc').limit(1).first
  end

  def previous
    Article.where('published_at < ?', published_at).order('published_at desc').limit(1).first
  end

  def self.find_by_published_at
    result = select('published_at').where('published_at is not NULL').where(type: 'Article')
    result.map{ |d| [d.published_at.strftime('%Y-%m')]}.uniq
  end

  # Finds one article which was posted on a certain date and matches the supplied dashed-title
  # params is a Hash
  def self.find_by_permalink(params)
    date_range = PoffyTime.delta(params[:year], params[:month], params[:day])

    req_params = {}
    req_params[:permalink] = params[:title] if params[:title]
    req_params[:published_at] = date_range if date_range

    return nil if req_params.empty? # no search if no params send
    article = find_published(:first, :conditions => req_params)
    return article if article

    if params[:title]
      req_params[:permalink] = CGI.escape(params[:title])
      article = find_published(:first, :conditions => req_params)
      return article if article
    end

    raise ActiveRecord::RecordNotFound
  end

  # Fulltext searches the body of published articles
  def self.search(query, args={})
    query_s = query.to_s.strip
    if !query_s.empty? && args.empty?
      Article.searchstring(query)
    elsif !query_s.empty? && !args.empty?
      Article.searchstring(query).page(args[:page]).per(args[:per])
    else
      []
    end
  end

  #def keywords_to_tags
  #  Tag.create_from_article!(self)
  #end

  def interested_users
    #User.find_all_by_notify_on_new_articles(true)
    User.where(notify_on_new_articles: true).all
  end

  def notify_user_via_email(user)
    if user.notify_via_email?
      EmailNotify.send_article(self, user)
    end
  end

  def comments_closed?
    ! allow_comments?
  end

  def html_urls
    urls = Array.new
    html.gsub(/<a\s+[^>]*>/) do |tag|
      if(tag =~ /\bhref=(["']?)([^ >"]+)\1/)
        urls.push($2.strip)
      end
    end
    urls.uniq
  end

  def cast_to_boolean(value)
    ActiveRecord::ConnectionAdapters::Column.value_to_boolean(value)
  end

  # Cast the input value for published= before passing it to the state.
  def published=(newval)
    state.published = cast_to_boolean(newval)
  end

  #def add_category(category, is_primary = false)
  #  self.categorizations.build(:category => category, :is_primary => is_primary)
  #end

  def access_by?(user)
    user.admin? || user_id == user.id
  end

  def allow_comments?
    return self.allow_comments unless self.allow_comments.nil?
    blog.default_allow_comments
  end

  protected

  def ensure_settings_type
    if settings.is_a?(String)
      # Any dump access forcing de-serialization
      true
    end
  end

  private

  def should_generate_new_friendly_id?
    super() && published?
  end

end
