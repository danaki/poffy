feed.entry item, :id => "urn:uuid:#{item.guid}", :url => item.permalink_url do |entry|
  entry.author do
    name = item.user.name rescue item.author
    email = item.user.email rescue nil
    entry.name name
    entry.email email if this_blog.link_to_author unless email.blank?
  end

  entry.title item.title, "type"=>"html"

  if item.is_a?(Article)

    item.themes.each do |theme|
      entry.theme "term" => theme.title, "label" => theme.title, "scheme" => theme.permalink_url
    end
    item.tags.each do |tag|
      entry.category "term" => tag.title, "scheme" => tag.permalink_url
    end

    #item.resources.each do |resource|
    #  if resource.size > 0  # The Atom spec disallows files with size=0
    #    entry.tag! :link, "rel" => "enclosure",
    #          :type => resource.mime,
    #          :title => item.title,
    #          :href => this_blog.file_url(resource.upload_url),
    #          :length => resource.size
    #  else
    #    entry.tag! :link, "rel" => "enclosure",
    #          :type => resource.mime,
    #          :title => item.title,
    #          :href => this_blog.file_url(resource.upload_url)
    #  end
    #end
  end

  entry.content html(item, :excerpt) + item.get_rss_description, "type"=>"html"
end

