<%= remotipart_response do %>
  root = exports ? this

  <% url = "/#{@up.store_dir}/#{File.basename(@up.path)}" %>

#  $('#preview_crop_image').attr 'src', '<%= url %>'
  $('#preview_original_image').val '/<%= File.basename(@up.path) %>'

#  $.lazybox.show($('#preview_crop_image'))
  $.lazybox.show($('<img id="preview_crop_image">').attr 'src', '<%= url %>')

  do $('#preview_crop_image').toggle
  do $('#preview_crop_form').toggle

  options =
    onChange: jcrop_save_constrains,
    onSelect: jcrop_save_constrains,
    aspectRatio: <%= @w %> / <%= @h %>,
    setSelect: [0, 0, <%= @w %>, <%= @h %>]
    onDblClick: ->
      c = root.jcrop_api.tellSelect()
      jcrop_save_constrains(c)
      do $('#preview_crop_form').submit
#      $('#lazy_close').trigger 'click'
      $.lazybox.close()
      do jcrop_api.release
      do jcrop_api.disable
      do $('#lazy_body').empty
      $('#preview_container img').attr 'src', '//:0'
      $('#preview_container img').after($('<i class="fa fa-spinner fa-spin"></i>'))
      do $('#preview_container').show
      toggle_preview(true)

  $('#preview_crop_image').Jcrop options,
    () -> root.jcrop_api = this

#  ready = ->
#    $('#upload_filename')
#      .on 'change', () ->
#        console.log '2'
#        do $('#preview_upload_form').submit

  $(document).ready(ready)
  $(document).on('page:load', ready)

<% end %>