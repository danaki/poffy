<%= remotipart_response do %>
  <% url = @resource.path(:full) %>

  root = exports ? this

  do root.jcrop_api.release
  do root.jcrop_api.disable

  do $('#preview_crop_form').toggle

  $('#preview_container img').attr 'src', '/<%= url %>'
  $('#preview_container a').attr 'href', '/<%= url %>'
  $('#preview_container img').next().remove()

  $('[name$=\\[resource_id\\]]').val <%= @resource.id %>
<% end %>