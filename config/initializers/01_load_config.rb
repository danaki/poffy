
class PoffyConfig < ActiveSupport::OrderedOptions
  def initialize
    file_merge("#{Rails.root}/config/sites/defaults.yml")

    filename = Rails.env + (if Rails.env == "production" then "_#{ENV['SITE']}" end).to_s
    path = "#{Rails.root}/config/sites/#{filename}.yml"

    file_merge(path)
  end

  def file_merge(path)
    c = config_merge(YAML.load(ERB.new(File.read(path)).result))
    self.deep_merge!(c)
  end

  def class_from_string(str)
    str.split('::').inject(Object) do |mod, class_name|
      mod.const_get(class_name)
    end
  end

  def parse_keyval(key, value)
    {key.to_sym => value}
  end

  def config_merge(config)
    result = ActiveSupport::OrderedOptions.new

    config.each do |key, value|

      if value.is_a? Hash
        r = {key.to_sym => config_merge(value)}
      else
        r = parse_keyval(key, value)
      end

      result.merge!(r)
    end

    result
  end
end

c = PoffyConfig.new

require_or_load("#{Rails.root}/config/sites/products/#{c.products._model.underscore}.rb")
c.products.model = c.class_from_string(c.products._model)

Rails.configuration.poffy = c

