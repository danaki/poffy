translations = {}
I18n.backend = I18n::Backend::Chain.new(I18n::Backend::KeyValue.new(translations), I18n.backend)

if Rails.configuration.poffy[:i18n].present? then
  Rails.configuration.poffy[:i18n].each do |locale, h|
    I18n.backend.store_translations(locale, h, :escape => false)
  end
end
