Rails.application.config.assets.precompile += [
  'administration/administration.js',
  'administration/administration.css',
  'application/tinymce.css',
  'tinymce/*'
]

Rails.application.config.assets.paths << Rails.root.join("app", "assets")
Rails.application.config.assets.paths << Rails.root.join("vendor", "themes", Rails.configuration.poffy.theme)
Rails.application.config.assets.paths << Rails.root.join("vendor", "shared")

