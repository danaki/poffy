require File.expand_path('../boot', __FILE__)

require 'rails/all'

class Class
  def extend?(klass)
    not superclass.nil? and (superclass == klass or superclass.extend? klass)
  end
end

#require 'figaro/application'
#require 'figaro/rails/application'
#require 'figaro/env'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Poffy
  class Application < Rails::Application
    raise 'SITE env variable must be set in production mode' if Rails.env.production? && ENV['SITE'].nil?

    # don't generate RSpec tests for views and helpers
    config.generators do |g|
      g.test_framework :rspec, fixture: true
      g.fixture_replacement :factory_girl, dir: 'spec/factories'

      g.view_specs false
      g.helper_specs false
    end

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.i18n.default_locale = :en
    config.i18n.enforce_available_locales = true
    #config.i18n.backend = I18n::Backend::Chain.new(I18n::Backend::KeyValue.new({'a' => 'b'}), config.i18n.backend)

    #translations = {}
    #I18n.backend = I18n::Backend::KeyValue.new(translations)
    #I18n.backend.store_translations(:ru, { 'test' => 'test' }, :escape => false)

    config.action_mailer.default_url_options = { :host => 'localhost' }

    #config.autoload_paths += Dir[Rails.root.join('app', 'models', 'products', '{**/}')]
    #config.eager_load_paths += Dir[Rails.root.join('app', 'models', 'products', '{**/}')]
    #puts Rails.configuration
    #config.autoload_paths += Dir[Rails.root.join('app', 'models', 'products', Rails.configuration.poffy.products.model.class)]

    config.assets.enabled = true
    # config.assets.paths.shift
    # puts config.assets.precompile.clear

    # config.assets.paths.shift

    #config.assets.paths << "#{Rails.root}/app/assets/poffy/stylesheets"

    # for tinymce
    # config.assets.paths << "#{Rails.root}/app/assets"
    # config.assets.paths << "#{Rails.root}/vendor/shared"
    # config.assets.paths << "#{Rails.root}/app/assets/ahred"
    # config.assets.paths << "#{Rails.root}/app/themes"

    # config.assets.paths << Rails.root.join("vendor", "themes", Rails.configuration.poffy.theme)

    # environment = Sprockets::Environment.new
    # environment.append_path Rails.root.join("public", "themes")

    # initializer 'setup_asset_pipeline', :group => :all  do |app|
    #   # We don't want the default of everything that isn't js or css, because it pulls too many things in
    #   app.config.assets.precompile.shift
    #   # app.config.assets.precompile.push(Proc.new do |path|
    #   #   File.extname(path).in? ['css', 'scss']
    #   #   end)
    # end
  end


  # Load included libraries.
  #require 'localization'
  require 'poffy_version'
  require 'global_slug_validator'
  require 'email_notify'
  require 'format'
  require 'route_cache'
  require 'stateful'
  require 'transforms'
  require 'poffy_guid'
  ## Required by the plugins themselves.
  # require 'publify_plugins'
  require 'bare_migration'

  Date::DATE_FORMATS.merge!(
    :long_weekday => '%a %B %e, %Y %H:%M'
  )

  ActionMailer::Base.default :charset => 'utf-8'

  if ::Rails.env != 'test'
    begin
      mail_settings = YAML.load(File.read("#{::Rails.root.to_s}/config/mail.yml"))

      ActionMailer::Base.delivery_method = mail_settings['method']
      ActionMailer::Base.server_settings = mail_settings['settings']
    rescue
      # Fall back to using sendmail by default
      ActionMailer::Base.delivery_method = :sendmail
    end
  end
end
