
class SlugMatcher
  def initialize(klass)
    @klass = klass
  end

  def matches?(request)
    record = @klass.find_by_slug(request.params[:id])
    ! record.nil?
  end
end

class HierarchialSlugMatcher
  def initialize(klass)
    @klass = klass
  end

  def matches?(request)
    roots = @klass.roots
    parts = if request.params[:parent_id]
      [request.params[:parent_id], request.params[:id]]
    else
      request.params[:id].split('/')
    end

    for part in parts
      record = roots.find_by_slug(part)
      break if record.nil?
      roots = record.children
    end

    ! record.nil?
  end
end

Rails.application.routes.draw do
  themes_for_rails
  devise_for :users

  authenticate :user do
    mount RailsExceptionHandlerAdmin::Engine, :at => '/errors'
  end

  #devise_scope :user do
  #    get '/login' => 'devise/sessions#new'
  #    get '/logout' => 'devise/sessions#destroy'
  #end
  resources :user, :controller => "user"

  # TODO: use only in archive sidebar. See how made other system
  get ':year/:month', :to => 'articles#index', :year => /\d{4}/, :month => /\d{1,2}/, :as => 'articles_by_month', :format => false
  get ':year/:month/page/:page', :to => 'articles#index', :year => /\d{4}/, :month => /\d{1,2}/, :as => 'articles_by_month_page', :format => false
  get ':year', :to => 'articles#index', :year => /\d{4}/, :as => 'articles_by_year', :format => false
  get ':year/page/:page', :to => 'articles#index', :year => /\d{4}/, :as => 'articles_by_year_page', :format => false

  resources :dashboard, only: [:index], module: 'admin', path: '/admin'

  get 'articles.:format', to: 'articles#index', constraints: {:format => 'rss'}, as: 'rss'
  get 'articles.:format', to: 'articles#index', constraints: {:format => 'atom'}, as: 'atom'

  #scope :controller => 'xml', :path => 'xml', :as => 'xml' do
  #  get 'articlerss/:id/feed.xml', :action => 'articlerss', :format => false
  #end

  #get 'xml/:format', :to => 'xml#feed', :type => 'feed', :constraints => {:format => 'rss'}, :as => 'xml'
  #get 'sitemap.xml', :to => 'xml#feed', :format => 'googlesitemap', :type => 'sitemap', :as => 'xml'

  get 'sitemap.xml', to: 'xml#feed', format: 'googlesitemap', type: 'sitemap'

  # scope :controller => 'xml', :path => 'xml', :as => 'xml' do
  #   scope :action => 'feed' do
  #     get ':format/feed.xml', :type => 'feed'
  #     get ':format/:type/:id/feed.xml'
  #     get ':format/:type/feed.xml'
  #   end
  # end

  #get 'xml/rsd', :to => 'xml#rsd', :format => false
  get 'xml/feed', :to => 'xml#feed'

  # CommentsController
  # resources :comments, :as => 'admin_comments' do
  #   collection do
  #     get :preview
  #   end
  # end

  # ArticlesController
  get '/live_search/', :to => 'articles#live_search', :as => :live_search_articles, :format => false
  #get '/search/:q(.:format)/page/:page', :to => 'articles#search', :as => 'search'
  #get '/search(/:q(.:format))', :to => 'articles#search', :as => 'search'
  get '/search/', :to => 'articles#search', :as => :search_base, :format => false
  get '/archives/', :to => 'articles#archives', :format => false
  get '/page/:page', :to => 'articles#index', :page => /\d+/, :format => false
  get '/pages/*name', :to => 'articles#view_page', :format => false

  # CategoriesController (imitate inflected_resource)
  resources :themes, :except => [:show, :update, :destroy, :edit]
  resources :themes, :path => 'theme', :only => [:edit, :update, :destroy]

  get '*id/page/:page', to: 'themes#show', :format => false, :constraints => SlugMatcher.new(Theme)
  get '*id', to: 'themes#show', :format => false, :constraints => SlugMatcher.new(Theme)

  # TagsController (imitate inflected_resource)
  resources :tags, :except => [:show, :update, :destroy, :edit]
  resources :tags, :path => 'tag', :only => [:edit, :update, :destroy]
  get '/tags/page/:page', :to => 'tags#index', :format => false

  get '*id/page/:page', to: 'tags#show', :format => false, :constraints => SlugMatcher.new(Tag)
  get '*id', to: 'tags#show', :format => false, :constraints => SlugMatcher.new(Tag)

  get '*parent_id/*id', to: 'products#index', :format => false, :constraints => HierarchialSlugMatcher.new(ProductCategory)
  get '*id', to: 'products#index', :format => false, :constraints => HierarchialSlugMatcher.new(ProductCategory)

  get '*id', to: 'articles#view_page', :format => false, :constraints => SlugMatcher.new(Page)

  # AuthorsController
  #get '/author/:id(.:format)', :to => 'authors#show', :format => /rss|atom/, :as => 'xml'
  #get '/author(/:id)', :to => 'authors#show', :format => false
  #resources :authors

  # For the tests
  get 'theme/static_view_test', :format => false

  # Work around the Bad URI bug
  #%w{ files }.each do |i|
  #  get "#{i}", :to => "#{i}#index", :format => false
  #  get "#{i}(/:action)", :to => i, :format => false
  #  get "#{i}(/:action(/:id))", :to => i, :id => nil, :format => false
  #end

  resources :articles do
    get 'preview'
    get 'view_page'
    get 'recent', on: :collection
    get 'preview_page'
  end

  resources :products, :except => [:index, :show]
  match '/contact', to: 'contact#deliver', via: 'post'

  #unless Rails.configuration.poffy.products.url.nil?
  #  get Rails.configuration.poffy.products.url, :to => "products#index"
  #end
  get "*id" => "products#show", :constraints => SlugMatcher.new(Rails.configuration.poffy.products.model)

  namespace :admin do
    resources :articles do
      collection do
        post :autosave
      end
      #post :destroy, on: :member
      #get :auto_complete_for_article_keywords, on: :collection
#      get :attachment_box_add, on: :member
    end

    resources :blogs do
      collection do
        get :index
        get :integration
        get :display
        get :feedback
        get :seo
        get :titles
        get :slugs
        patch :update
      end
    end

    resources :resources do
      collection do
        get :get_thumbnails
        post :upload
        post :wysiwyg_upload
      end
    end

    resources :users
    resources :pages
    resources :products
    resources :redirects
    #resources :tags
    #resources :themes
    #resources :cache
    resources :post_types

    resources :tags do post :rebuild, on: :collection end
    resources :themes do post :rebuild, on: :collection end
    resources :product_categories do post :rebuild, on: :collection end
    resources :menus do post :rebuild, on: :collection end
  end

  # default
  root :to  => 'articles#index', :format => false

  unless Rails.configuration.poffy.pages.nil?
    get "*id" => "high_voltage/pages#show", :constraints => lambda{|req| Rails.configuration.poffy.pages.include?(req.params[:id]); }
  end

  get '*from', :to => 'articles#redirect', :format => false
end
