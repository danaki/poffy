require 'rubygems'

# Set up gems listed in the Gemfile.
gemfile = File.expand_path('../../Gemfile', __FILE__)
begin
  ENV['BUNDLE_GEMFILE'] = gemfile
  require 'bundler'
  Bundler.setup
rescue Bundler::GemNotFound => e
  STDERR.puts e.message
  STDERR.puts "Try running `bundle install`."
  exit!
end if File.exist?(gemfile)

#require 'rails/commands/server'
#
#module DefaultOptions
#  def default_options
#    super.merge!(Port: 3000)
#  end
#end
#
#Rails::Server.send(:prepend, DefaultOptions)