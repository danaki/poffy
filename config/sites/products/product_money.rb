class ProductMoney < Product
  belongs_to :user
  belongs_to :resource

  after_save :shorten_url

  self.init_url_helpers(self)

  def self.default_order
    "settings -> 'prio' ASC"
  end

  def self.search_with(search_hash)
    super(search_hash).order('title ASC')
  end

end
