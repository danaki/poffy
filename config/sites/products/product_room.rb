class ProductRoom < Product
  include ConfigManager

  setting :screenshot_small, :image
  setting :screenshot, :image
  # setting :network, :string
  setting :bonuscode, :string
  setting :software, :string
  # setting :bonus, :string
  setting :nodep_bonus, :string
  setting :rake, :string
  setting :custom_header, :boolean
  setting :show_on_homepage, :boolean

  belongs_to :user
  belongs_to :resource

  self.init_url_helpers(self)

  def self.search_with(search_hash)
    super(search_hash).order('title ASC')
  end

end
