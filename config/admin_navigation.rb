SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = SimpleNavigationRenderers::Bootstrap3

  navigation.items do |primary|
    primary.item :dashboard, {text: 'Dashboard', icon: "fa fa-dashboard"}, url_for(controller: 'admin/dashboard')
    primary.dom_class = 'nav navbar-nav side-nav'

    if can?(:read, Article) ||
      can?(:read, Theme) ||
      can?(:read, Tag) ||
      can?(:read, PostType) ||
      can?(:read, Redirect) then
        primary.item :articles, {text: 'Content', icon: 'fa fa-bars'} do |secondary|
          secondary.item :articles_index,   'Articles',      url_for(controller: 'admin/articles',   action: 'index') if can? :read, Article
          secondary.item :articles_index,   'Pages',         url_for(controller: 'admin/pages',      action: 'index') if can? :read, Page
          secondary.item :themes_index,     'Themes',        url_for(controller: 'admin/themes',     action: 'new') if can? :read, Theme
          secondary.item :tags_index,       'Tags',          url_for(controller: 'admin/tags',       action: 'new') if can? :read, Tag
          #secondary.item :post_types_index, 'Product Types', url_for(controller: 'admin/post_types', action: 'new') if can? :read, PostType
          secondary.item :redirects_index,  'Redirects',     url_for(controller: 'admin/redirects',  action: 'new') if can? :read, Redirect
        end
    end

    #if can?(:read, Page)
    #  primary.item :pages, 'Pages', url_for(controller: 'admin/pages', action: 'index'), icon: 'fa fa-table'
    #end

    if can?(:read, Product)
      primary.item :products, {text: 'Products', icon: 'fa fa-shopping-cart'} do |secondary|
        secondary.item :products_index,      'Products',   url_for(controller: 'admin/products',           action: 'index')
        secondary.item :products_categories, 'Categories', url_for(controller: 'admin/product_categories', action: 'index')
      end
    end

    if can?(:read, Menu)
      primary.item :menus, {text: 'Menu', icon: 'fa fa-list'}, url_for(controller: 'admin/menus', action: 'index')
    end

    if can?(:read, User)
      primary.item :users, {text: 'Users', icon: 'fa fa-users'}, url_for(controller: 'admin/users', action: 'index')
    end

    if can?(:update, current_user)
      primary.item :users, {text: 'Profile', icon: 'fa fa-user'}, url_for(controller: 'admin/users', action: 'edit', id: current_user.id)
    end

    primary.item :blog, {text: 'Settings', icon: 'fa fa-wrench'} do |secondary|
      secondary.item :settings_index,       'General',     url_for(controller: 'admin/blogs', action: 'index')
      secondary.item :settings_integration, 'Integration', url_for(controller: 'admin/blogs', action: 'integration')
      secondary.item :settings_display,     'Display',     url_for(controller: 'admin/blogs', action: 'display')
    end if can?(:read, Blog)

    primary.item :seo, {text: 'SEO', icon: 'fa fa-external-link'} do |secondary|
      secondary.item :seo_index,  'Global SEO settings', url_for(controller: 'admin/blogs', action: 'seo')
      secondary.item :seo_titles, 'Titles',              url_for(controller: 'admin/blogs', action: 'titles')
      secondary.item :seo_slugs,  'Slugs',               url_for(controller: 'admin/blogs', action: 'slugs')
    end if can?(:read, Blog)
  end

end