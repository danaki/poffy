
module Validators
  class GlobalSlugValidator < ActiveRecord::Validations::UniquenessValidator
    def self.all_models
      Module.constants.select do |constant_name|
        constant = eval constant_name.to_s
        if not constant.nil? and constant.is_a? Class and constant.extend? ActiveRecord::Base
          constant
        end
      end
    end

    def self.models_with_slug
      all_models.select { |i| Module.const_get(i).column_names.include? "slug" }
    end

    def validate_each_with_globally(record, attribute, value)
      validate_each_without_globally(record, attribute, value)

      unless record.errors.count > 0
        models = self.class.models_with_slug
        superclasses = find_all_superclasses(record).collect { |c| c.name.to_sym }
        models.delete_if { |x| superclasses.include?(x) }

        models.each do |m|
          k = Module.const_get(m)
          table = k.arel_table
          relation = build_relation(k, table, attribute, value)
          relation = k.where(relation)

          if relation.exists?
            error_options = options.except(:case_sensitive, :scope, :conditions)
            error_options[:value] = value

            record.errors.add(attribute, "taken in #{k.name}", error_options)
          end
        end
      end
    end

    def find_all_superclasses(record)
      class_hierarchy = [record.class]

      while class_hierarchy.first.superclass != ActiveRecord::Base
        class_hierarchy.insert(0, class_hierarchy.first.superclass)
      end

      class_hierarchy.detect { |klass| !klass.abstract_class? }
      class_hierarchy
    end

    alias_method_chain :validate_each, :globally

  end
end