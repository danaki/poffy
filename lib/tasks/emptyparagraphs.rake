namespace :poffy do
  desc "Remove empty paragraphs in all posts"
  task :emptyparagraphs => :environment do
    Content.all.each do |a|
      a.update_attribute(:body, a.body
        .gsub(/<\/(h[1-5]|p|blockquote)>[\r\n\s]*<p[^>]*>[\r\n\s]*<\/p>/, '</\1>')
        .gsub(/<\/(h[1-5]|p|blockquote)>[\r\n\s]*<p[^>]*>[\r\n\s]*&nbsp;[\r\n\s]*<\/p>/, '</\1>')
        .gsub(/<\/(h[1-5]|p|blockquote)>[\r\n\s]*<p[^>]*>[\r\n\s]*<b>[\r\n\s]*&nbsp;[\r\n\s]*<\/b>[\r\n\s]*<\/p>/, '</\1>'))
    end
  end
end