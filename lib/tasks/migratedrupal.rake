# Run like this:
#     rake poffy:migratedrupal[mysql://root@localhost/apoker,ap_terms.tsv]

namespace :poffy do
  desc "Migrate"
  task :migratedrupal, [:drupaldsn, :termtsv] => :environment do |t, args|

    require 'action_view/helpers'
    require 'rubygems'

    class Helper
       include Singleton
       include ActionView::Helpers::SanitizeHelper
       include ActionView::Helpers::TextHelper

       extend ActionView::Helpers::SanitizeHelper::ClassMethods
    end

    raise 'Provide Drupal DSN in arguments like this "rake poffy:migratedrupal[adapter://user:password@host/database]"' if args[:drupaldsn].empty?

    if args[:termtsv].present? then
      require 'strict_tsv'

      term_theme_map = {}
      tsv = Vendor::StrictTsv.new(args[:termtsv])
      tsv.parse do |row|
        term_theme_map[row['id'].to_i] = row['theme']
      end
    end

    url = URI.parse(args[:drupaldsn])

    $config = {
      adapter: url.scheme,
      host: url.host,
      username: url.user,
      password: url.password,
      database: url.path[1..-1]
    }

    user = User.first

    require 'date'
    require 'safe_attributes/base'
    require 'drupal'

    # Get all the blog posts from Drupal
    nodes = Drupal::Node.where(type: ['paper']).order('nid ASC').all

    nodes.each do |node|
      # puts "Migrating: #{node.title}"

      article = Article.new

      body = Helper.instance.simple_format(node.revision.body.force_encoding('utf-8').gsub(/<!--.*-->/, ""))

      article.title = node.title
      article.slug = node.title.to_s.to_slug.normalize(transliterate: 'russian').to_s
      article.published = true
      article.user = user
      article.body = body
      article.created_at = DateTime.strptime(node[:created].to_s, '%s')
      article.updated_at = DateTime.strptime(node[:changed].to_s, '%s')
      article.published_at = DateTime.strptime(node[:changed].to_s, '%s')

      if term_theme_map.present? then
        node.terms.each do |term|
          if term_theme_map[term.id.to_i].present?
            themes = []
            term_theme_map[term.id.to_i].split(" ").each do |slug|
              theme = Theme.where(slug: slug, title: slug).first_or_create
              themes << theme
            end

            themes.uniq! { |t| t.id }
            article.themes = themes

            # puts article.themes.inspect

          end
        end
      end

      article.save

      aliases = Drupal::UrlAlias.find_aliases_for_node(node)
      aliases.each do |link|
        puts "/#{link.src} " + article.permalink_url(only_path: true)
        puts "/#{link.dst} " + article.permalink_url(only_path: true)
      end
    end
  end
end