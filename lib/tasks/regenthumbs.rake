namespace :poffy do
  desc "Regenerate thumbnails"
  task :regenthumbs => :environment do
    config = Resource.thumbnails_for('article')
    Article.all.each do |a|
      unless a.resource_id.nil?
        dir = "#{Resource.resource_dir}/#{a.resource_id.to_s}"
        dst_dir = "#{Rails.public_path}/#{dir}"

        ext = if File.file?("#{dst_dir}/crop.jpg")
                "jpg"
              elsif File.file?("#{dst_dir}/crop.png")
                "png"
              elsif File.file?("#{dst_dir}/crop.gif")
                "gif"
              else
                "jpeg"
              end

        blob = MiniMagick::Image.open("#{dst_dir}/crop.#{ext}")
        image = MiniMagick::Image.read(blob)

        config.each do |size, constraints|
          thumb = image.clone
          # thumb.resize(constraints)
          thumb.combine_options do |c|
            c.unsharp("2x0.5+0.5+0")
            c.resize(constraints)
            c.quality(100)
            # c.sharpen(1)
          end
          filename = "#{dst_dir}/#{size}.#{ext}"
          puts "Converting #{filename}"
          thumb.write(filename)
        end
      end
    end
  end
end