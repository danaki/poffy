
namespace :poffy do
  desc "fixcatdupes"
  task :fixcatdupes => :environment do |t, args|
    # sql = 'SELECT article_id, category_id, COUNT(*) AS cnt FROM categorizations GROUP BY article_id, category_id HAVING COUNT(*) > 1'
    # Categorization.find_by_sql(sql).each do |c| puts c.inspect end
    dupes = []
    Categorization.select(:article_id, :category_id).group(:article_id, :category_id).having("count(*) > 1").each do |c|
      dupes << [c.article_id, c.category_id]
    end
    dupes.each do |d|
      Categorization.where(article_id: d[0], category_id:d[1]).delete_all
      Categorization.new(article_id: d[0], category_id:d[1]).save
    end

    puts dupes.inspect
  end
end