
require 'rubygems'
require 'active_record'

module Drupal
  class DrupalDatabase < ActiveRecord::Base
    self.abstract_class = true
    establish_connection $config

    # logger = Logger.new(File.open('drupal.database.log', 'a'))
    def to_s
      attributes.each {|name, value|
        puts "#{name}: #{value}"
      }
    end
  end
  
  class User < DrupalDatabase
    self.table_name = 'users'
    self.primary_key = 'uid'
  end

  class Node < DrupalDatabase
    include SafeAttributes::Base

    self.table_name = 'node'
    self.primary_key = 'nid'
    self.inheritance_column = ''
    
    belongs_to :user, :class_name => 'Drupal::User', :foreign_key => 'uid'
    
    has_many :comments, :class_name => 'Drupal::Comment', :foreign_key => 'nid'

    has_one :revision, :class_name => 'Drupal::NodeRevision', :primary_key => 'vid', :foreign_key => 'vid'

    has_and_belongs_to_many :terms, :association_foreign_key => 'tid',
                                    :foreign_key => 'nid',
                                    :join_table => 'term_node',
                                    :class_name => 'Drupal::Term'
  end

  class NodeRevision < DrupalDatabase
    include SafeAttributes::Base

    self.table_name = 'node_revisions'
    self.primary_key = 'vid'
    self.inheritance_column = ''

    belongs_to :node, :class_name => 'Drupal::Node', :foreign_key => 'nid'
  end

  class Term < DrupalDatabase
    self.table_name = 'term_data'
    self.primary_key = 'tid'
  end

  class Comment < DrupalDatabase
    self.primary_key = 'cid'
    belongs_to :node, :class_name => 'Drupal::Node', :foreign_key => 'nid'
  end
  
  class UrlAlias < DrupalDatabase
    self.table_name = 'url_alias'
    
    def self.find_aliases_for_node(node)
      self.where(src: "node/#{node.nid}").all
    end
  end
end
