# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Daley', city: cities.first)

#ActiveRecord::Base.logger = Logger.new(STDOUT)
#Blog.first.delete

blog = Blog.create(
  base_url: 'http://localhost'
)

blog.blog_name = 'Poffy Blog!'
blog.save

(1..5).each do |i|
  menu = Menu.create(
    title: 'Menu ' + i.to_s,
    link: '/'
  )

  (1..5).each do |j|
    Menu.create(
      title: 'Menu ' + i.to_s + '.' + j.to_s,
      link: '/',
      parent: menu
    )
  end

end

Dir.mkdir("#{::Rails.root.to_s}/public/files") unless File.directory?("#{::Rails.root.to_s}/public/files")

role = Role.where(name: 'admin').first_or_create

admin = User.create(
  username: 'admin',
  email: 'pimenoff@pokeroff.ru',
  password: 'admin',
  password_confirmation: 'admin',
  name: 'Admin',
  roles: [role]
)

admin.confirm!
admin.skip_confirmation!
admin.save
admin.add_role :admin
#
# theme = Theme.create(
#   title: 'General',
#   description: <<EOF
#     A long time ago, in a galaxy far, far away....<br>
#
#     It is a period of civil war. Rebel<br>
#     spaceships, striking from a hidden<br>
#     base, have won their first victory<br>
#     against the evil Galactic Empire.<br>
#
#     During the battle, Rebel spies managed<br>
#     to steal secret plans to the Empire's<br>
#     ultimate weapon, the Death Star, an<br>
#     armored space station with enough<br>
#     power to destroy an entire planet.<br>
#
#     Pursued by the Empire's sinister agents,<br>
#     Princess Leia races home aboard her<br>
#     starship, custodian of the stolen plans<br>
#     that can save her people and restore<br>
#     freedom to the galaxy...<br>
# EOF
# )
#
# tag = Tag.create(
#   title: 'A tag',
#   description: <<EOF
#     Tag <strong>description</strong>
# EOF
# )
#
# (1..20).each do |i|
#   Article.create(
#     title: 'Hello World ' + i.to_s + '!',
#     body: 'Welcome to Poffy. This is your first article. Edit or delete it, then start blogging!',
#     state: 'published',
#     allow_comments: 1,
#     published: 1,
#     themes: [theme],
#     tags: [tag],
#     user: admin
#   )
# end
