class FixCategoriesTagParentId < ActiveRecord::Migration
  def self.up
    tag_ids = Tag.all.collect(&:id)
    ActiveRecord::Base.connection.execute('SELECT * FROM categories WHERE type=\'Tag\'').each do |tag|
      unless tag_ids.include?(tag['parent_id'].to_i)
        q = "UPDATE categories SET parent_id=NULL WHERE id=#{tag['id']}"
        puts q
        ActiveRecord::Base.connection.execute(q)
      end
    end
    Theme.rebuild!
    Tag.rebuild!
  end
end
