class MergeCategoriesAndTagsToUniversal < ActiveRecord::Migration
  def qs(s)
    if s.nil?
      ""
    else
      s.gsub(/\\/, '\&\&').gsub(/'/, "''") # ' (for ruby-mode)
    end
  end

  def self.up
    Category.update_all("type = 'Theme'")
    base = Category.maximum('id').to_i
    ActiveRecord::Base.connection.execute('SELECT * FROM tags').each do |tag|
      id = (tag['id'].to_i+base).to_s
      parent_id = (tag['parent_id'].to_i+base).to_s
      lft = (tag['lft'].to_i+base).to_s
      rgt = (tag['rgt'].to_i+base).to_s

      ActiveRecord::Base.connection.execute("INSERT INTO categories (id,title,slug,description,parent_id,lft,rgt,type)
        VALUES (#{id}, '#{qs(tag['title'])}', '#{tag['slug']}', '#{qs(tag['description'])}', #{parent_id}, #{lft}, #{rgt}, 'NewTag')")
    end

    id = Categorization.maximum('id')
    ActiveRecord::Base.connection.execute('SELECT * FROM articles_tags').each do |at|
      category_id = (at['tag_id'].to_i+base).to_s
      id += 1
      ActiveRecord::Base.connection.execute("INSERT INTO categorizations (id,article_id,category_id)
        VALUES (#{id}, #{at['article_id']}, #{category_id})")
    end
  end
end
