class DropKeywordsFromCategories < ActiveRecord::Migration
  def change
    remove_column :categories, :keywords
  end
end
