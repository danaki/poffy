class AddNestedToTags < ActiveRecord::Migration
  def self.up
    #add_column :tags, :parent_id, :integer # Comment this line if your project already has this column
    # Category.where(parent_id: 0).update_all(parent_id: nil) # Uncomment this line if your project already has :parent_id
    add_column :tags, :lft, :integer
    add_column :tags, :rgt, :integer

    remove_column :tags, :position

    # This is necessary to update :lft and :rgt columns
    Tag.rebuild!
  end

  def self.down
    remove_column :tags, :parent_id
    remove_column :tags, :lft
    remove_column :tags, :rgt
    remove_column :tags, :depth  # this is optional.

    add_column :tags, :position, :integer
  end  
end
