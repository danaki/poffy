class RenameNamesToTitles < ActiveRecord::Migration
  def change
    rename_column :categories, :name, :title
    rename_column :tags, :name, :title
    rename_column :post_types, :name, :title
  end
end
