class RenameCategoryDescriptionToBody < ActiveRecord::Migration
  def change
    change_table :categories do |t|
      t.rename :description, :body
    end
  end
end
