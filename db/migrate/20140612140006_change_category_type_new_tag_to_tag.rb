class ChangeCategoryTypeNewTagToTag < ActiveRecord::Migration
  def change
    Category.where(type: 'NewTag').update_all(type: 'Tag')
  end
end
