class AddNestedToCategories < ActiveRecord::Migration
  def self.up
    #add_column :categories, :parent_id, :integer # Comment this line if your project already has this column
    # Category.where(parent_id: 0).update_all(parent_id: nil) # Uncomment this line if your project already has :parent_id
    add_column :categories, :lft, :integer
    add_column :categories, :rgt, :integer

    remove_column :categories, :position

    # This is necessary to update :lft and :rgt columns
    Category.rebuild!
  end

  def self.down
    remove_column :categories, :parent_id
    remove_column :categories, :lft
    remove_column :categories, :rgt
    remove_column :categories, :depth  # this is optional.

    add_column :categories, :position, :integer
  end  
end
