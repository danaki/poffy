class DropCategoriesSlugIndexUniqueness < ActiveRecord::Migration
  def change
    remove_index :categories, name: 'index_categories_on_slug'
    add_index :categories, [:slug], name: 'index_categories_on_slug'
  end
end
