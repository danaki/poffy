class RenameSettingsUrlToLinkedUrl < ActiveRecord::Migration
  def change
    Product.find_each do |p|
      p.linked_url = p.url
      p.url = nil
      p.save
    end
  end
end
