class InitialSchema < ActiveRecord::Migration
  def self.up
    enable_extension "hstore"
  end

  def self.down
    disable_extension "hstore"
  end

  def change
    create_table :articles_tags, id: false, force: true do |t|
      t.integer "article_id"
      t.integer "tag_id"
    end

    create_table :blogs, force: true do |t|
      t.hstore "settings"
      t.string "base_url"
    end

    create_table :categories, force: true do |t|
      t.string  "name"
      t.integer "position"
      t.string  "slug"
      t.text    "description"
      t.text    "keywords"
      t.integer "parent_id"
    end

    add_index :categories, [:slug], name: "index_categories_on_slug", unique: true, using: :btree

    create_table :categorizations, force: true do |t|
      t.integer "article_id"
      t.integer "category_id"
      t.boolean "is_primary"
    end

    create_table :tags, force: true do |t|
      t.string   "name"
      t.integer  "position"
      t.string   "slug"
      t.text     "description"
      t.integer  "parent_id"
    end

    add_index :tags, [:slug], name: "index_tags_on_slug", unique: true, using: :btree

    create_table :contents, force: true do |t|
      t.string   "type"
      t.string   "title"
      t.string   "author"
      t.text     "body"
      t.text     "excerpt"
      t.integer  "user_id"
      t.string   "slug"
      t.string   "guid"
      t.boolean  "published",      default: false
      t.boolean  "allow_comments"
      t.datetime "published_at"
      t.string   "state"
      t.integer  "parent_id"
      t.integer  "resource_id"
      t.string   "post_type",      default: "read"
      t.hstore   "settings"
      t.timestamps
    end

    add_index :contents, ["published"], name: "index_contents_on_published", using: :btree
    add_index :contents, [:slug], name: "index_contents_on_slug", unique: true, using: :btree

    #create_table :page_caches, force: true do |t|
    #  t.string "name"
    #end
    #
    #add_index :page_caches, ["name"], name: "index_page_caches_on_name", using: :btree

    create_table :post_types, force: true do |t|
      t.string "name"
      t.string "slug"
      t.string "description"
    end

    add_index :post_types, [:slug], name: "index_post_types_on_slug", unique: true, using: :btree

    create_table :profiles, force: true do |t|
      t.string "label"
      t.string "nicename"
      t.text   "modules"
    end

    create_table :profiles_rights, id: false, force: true do |t|
      t.integer "profile_id"
      t.integer "right_id"
    end

    create_table :redirections, force: true do |t|
      t.integer "content_id"
      t.integer "redirect_id"
    end

    create_table :redirects, force: true do |t|
      t.string   "from_path"
      t.string   "to_path"
      t.string   "origin"
      t.timestamps
    end

    create_table :resources, force: true do |t|
      t.string   "ext"
      t.string   "mime"
      t.timestamps
    end

    create_table :sidebars, force: true do |t|
      t.integer "active_position"
      t.text    "config"
      t.integer "staged_position"
      t.string  "type"
    end

    create_table :sitealizer, force: true do |t|
      t.string   "path"
      t.string   "ip"
      t.string   "referer"
      t.string   "language"
      t.string   "user_agent"
      t.timestamps
    end

    create_table :triggers, force: true do |t|
      t.integer  "pending_item_id"
      t.string   "pending_item_type"
      t.datetime "due_at"
      t.string   "trigger_method"
    end

    create_table :users, force: true do |t|
      t.string   "username"
      t.string   "email"
      t.text     "name"
      t.boolean  "notify_via_email"
      t.boolean  "notify_on_new_articles"
      t.integer  "profile_id"
      t.string   "state", default: "active"
      t.hstore   "settings"
      t.integer  "resource_id"
      t.string   "provider"
      t.string   "uid"
    end

    create_table :menus, force: true do |t|
      t.string  "title"
      t.string  "link"
      t.integer "position"
      t.integer "parent_id"
    end

  end

end
