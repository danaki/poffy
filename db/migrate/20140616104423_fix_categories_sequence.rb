class FixCategoriesSequence < ActiveRecord::Migration
  def self.up
    max = Category.maximum('id').to_i + 1
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE categories_id_seq RESTART WITH #{max}")
  end
end
