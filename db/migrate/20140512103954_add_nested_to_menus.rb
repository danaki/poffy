class AddNestedToMenus < ActiveRecord::Migration
  def self.up
    #add_column :menus, :parent_id, :integer # Comment this line if your project already has this column
    # Category.where(parent_id: 0).update_all(parent_id: nil) # Uncomment this line if your project already has :parent_id
    add_column :menus, :lft, :integer
    add_column :menus, :rgt, :integer

    remove_column :menus, :position

    # This is necessary to update :lft and :rgt columns
    Menu.rebuild!
  end

  def self.down
    remove_column :menus, :parent_id
    remove_column :menus, :lft
    remove_column :menus, :rgt
    remove_column :menus, :depth  # this is optional.

    add_column :menus, :position, :integer
  end  
end
