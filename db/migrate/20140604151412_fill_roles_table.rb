class FillRolesTable < ActiveRecord::Migration
  def change
    Role.where(name: 'admin').first_or_create
    Role.where(name: 'publisher').first_or_create
  end
end
