class DropOldTags < ActiveRecord::Migration
  def change
    drop_table :tags
    drop_table :articles_tags
  end
end
