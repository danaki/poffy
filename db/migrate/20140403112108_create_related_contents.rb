class CreateRelatedContents < ActiveRecord::Migration
  def change
    create_table :related_contents, force: true do |t|
      t.integer "article_id"
      t.integer "related_content_id"
    end
  end
end
