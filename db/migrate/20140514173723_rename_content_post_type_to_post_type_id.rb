class RenameContentPostTypeToPostTypeId < ActiveRecord::Migration
  def change
    #post_types = PostType.all
    #map = post_types.to_a.each_with_object({}) { |c,h| h[c.title] = c }

    add_column :contents, :post_type_id, :integer

    #Content.all.each do |c|
    #  c.post_type_id = map[c.post_type].id
    #  c.save!
    #end

    remove_column :contents, :post_type
  end
end
