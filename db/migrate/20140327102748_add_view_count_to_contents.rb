class AddViewCountToContents < ActiveRecord::Migration
  def change
    add_column :contents, :view_count, :integer, null: false, default: 0
  end
end
