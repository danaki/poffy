class AddIndexByPublishedAtToContents < ActiveRecord::Migration
  def change
    add_index :contents, :published_at, name: "index_contents_on_published_at"
  end
end
