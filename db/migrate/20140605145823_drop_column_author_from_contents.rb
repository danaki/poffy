class DropColumnAuthorFromContents < ActiveRecord::Migration
  def change
    remove_column :contents, :author
  end
end
