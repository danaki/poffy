class DropProfiles < ActiveRecord::Migration
  def change
    drop_table :profiles
    drop_table :profiles_rights
  end
end
