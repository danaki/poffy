class AddSettingsToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :settings, :hstore
  end
end
