class MoveCategoryDescriptionToMetaDescription < ActiveRecord::Migration
  def change
    Category.find_each do |c|
      c.meta_description = c.description
      c.save
    end
  end
end
