class FixCategorizationsSequence < ActiveRecord::Migration
  def self.up
    max = Categorization.maximum('id').to_i + 1
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE categorizations_id_seq RESTART WITH #{max}")
  end
end
