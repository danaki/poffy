# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141110164143) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "blogs", force: true do |t|
    t.hstore "settings"
    t.string "base_url"
  end

  create_table "categories", force: true do |t|
    t.string  "title"
    t.string  "slug"
    t.text    "body"
    t.integer "parent_id"
    t.integer "lft"
    t.integer "rgt"
    t.string  "type"
    t.hstore  "settings"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", using: :btree

  create_table "categorizations", force: true do |t|
    t.integer "article_id"
    t.integer "category_id"
    t.boolean "is_primary"
  end

  create_table "contents", force: true do |t|
    t.string   "type"
    t.string   "title"
    t.text     "body"
    t.text     "excerpt"
    t.integer  "user_id"
    t.string   "slug"
    t.string   "guid"
    t.boolean  "published",      default: false
    t.boolean  "allow_comments"
    t.datetime "published_at"
    t.string   "state"
    t.integer  "parent_id"
    t.integer  "resource_id"
    t.hstore   "settings"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "view_count",     default: 0,     null: false
    t.integer  "post_type_id"
    t.text     "description"
  end

  add_index "contents", ["published"], name: "index_contents_on_published", using: :btree
  add_index "contents", ["published_at"], name: "index_contents_on_published_at", using: :btree
  add_index "contents", ["slug"], name: "index_contents_on_slug", unique: true, using: :btree

  create_table "error_messages", force: true do |t|
    t.text     "class_name"
    t.text     "message"
    t.text     "trace"
    t.text     "params"
    t.text     "target_url"
    t.text     "referer_url"
    t.text     "user_agent"
    t.string   "user_info"
    t.string   "app_name"
    t.string   "doc_root"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menus", force: true do |t|
    t.string  "title"
    t.string  "link"
    t.integer "parent_id"
    t.integer "lft"
    t.integer "rgt"
  end

  create_table "post_types", force: true do |t|
    t.string "title"
    t.string "slug"
    t.text   "description"
  end

  add_index "post_types", ["slug"], name: "index_post_types_on_slug", unique: true, using: :btree

  create_table "redirections", force: true do |t|
    t.integer "content_id"
    t.integer "redirect_id"
  end

  create_table "redirects", force: true do |t|
    t.string   "from_path"
    t.string   "to_path"
    t.string   "origin"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "related_contents", force: true do |t|
    t.integer "article_id"
    t.integer "related_content_id"
  end

  create_table "resources", force: true do |t|
    t.string   "ext"
    t.string   "mime"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "sitealizer", force: true do |t|
    t.string   "path"
    t.string   "ip"
    t.string   "referer"
    t.string   "language"
    t.string   "user_agent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "triggers", force: true do |t|
    t.integer  "pending_item_id"
    t.string   "pending_item_type"
    t.datetime "due_at"
    t.string   "trigger_method"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "email"
    t.text     "name"
    t.boolean  "notify_via_email"
    t.boolean  "notify_on_new_articles"
    t.string   "state",                  default: "active"
    t.hstore   "settings"
    t.integer  "resource_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "encrypted_password",     default: "",       null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,        null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
