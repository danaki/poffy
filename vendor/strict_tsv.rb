module Vendor

  class StrictTsv
    attr_reader :filepath
    attr_reader :have_headers

    def initialize(filepath, have_headers = true)
      @filepath = filepath
      @have_headers = have_headers
    end

    def parse
      open(filepath) do |f|
        headers = f.gets.strip.split("\t") if @have_headers

        f.each do |line|
          fields = line.strip.split("\t")
          fields = Hash[headers.zip(fields)] if @have_headers

          yield fields
        end
      end
    end
  end
end